# Unterrichtsressourcen

## [0 - Einführung in Microservices](0_Einführung/README.md)

- Einführung in die Grundkonzepte und die Bedeutung von Microservices für Unternehmen.
- Praktische Anwendung und Herausforderungen von Microservices in der realen Welt.
- Detaillierte Erläuterung der Architektur und Funktionsweise von Microservices.
- Überblick über die technologischen Komponenten, die zur Entwicklung von Microservices benötigt werden.

## [A - Entwicklungsumgebung](A_Entwicklungsumgebung/README.md)

- Einführung in Docker Desktop als zentrales Tool für die Microservice-Entwicklung.
- Nutzung von Linux-Umgebungen unter Windows für die Entwicklung von Microservices.
- Versionskontrolle und kollaborative Entwicklung mit Git und GitLab.
- Auswahl und Einrichtung einer geeigneten Python-IDE für die Entwicklung von Microservices.

## [B - Microservices lokal](B_Microservices_lokal/README.md)

- Erstellung eines minimalen, dockerisierten Flask-Microservices.
- Testen und Entwickeln von RESTful APIs mit Postman.
- Implementierung von Persistenz in Microservices mithilfe von MySQL.
- Aufbau und Optimierung einer skalierbaren Microservice-Architektur.

## [C - Towards Production](C_Towards_Production/README.md)

- Entwicklung von Teststrategien und Durchführung automatisierter Tests.
- Erstellen von Produktions-Images für den Einsatz in einer produktiven Umgebung.
- Durchführung und Validierung von manuellen Deployment-Prozessen.
- Integration von GitLab CI/CD für automatisierte Deployment-Workflows.
- Automatisierung von Deployment-Prozessen und Produktionsüberwachung.

## [D - Advanced Topics](D_Advanced_Topics/README.md)

- Implementierung von Sicherheits- und Authentifizierungsmechanismen in Microservices.
- Überwachung und Performance-Optimierung von Microservices im Betrieb.
- Spezielle Themen und Herausforderungen in der Microservice-Welt.
- Benutzeroberflächen und deren Interaktion mit Microservices.
- Best Practices und Design Patterns für Microservices.
- Einsatz von API Gateways und GraphQL zur Optimierung von APIs.
- Implementierung asynchroner Kommunikationsmuster in Microservice-Architekturen.

