# C - Towards Production

[[_TOC_]]

Jetzt bewegen wir uns mit unserem Microservice langsam in Richtung Cloud-Setup.


## Kompetenzen

- B10.1 Die Architektur der Software bestimmen und die Entwicklung unter Berücksichtigung von Betrieb und Wartung planen und dokumentieren (Niveau: 3)

## Testing

Wir setzen mit pytest eine Test-Umgebung auf, die sicherstellt, dass wir einen qualitativ hochwertigen Microservice veröffentlichen.

### Hands on

[Testing](./01_testing.md)

## Production 

Unter Verwendung von Flask und Docker wird ein production Image für einen Microservices erstellt.

### Hands on

[Production Image](./02_production_image.md)

## Manuelles Deployment

Mit AWS Academy erstellen wir eine EC2 Instanz und installieren unseren Microservice manuell.

### Hands on

[Deployment (Manuell)](./03_manual_deployment.md)



## Links

*Allgemeine Links welche sich nicht in die Unterthemen einordnen lassen*

