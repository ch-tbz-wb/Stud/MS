# CI/CD Automatisierung

## Optionen

Es gibt immer mehr Service Provider, die Hosting und Deployment anbieten. In der Regel vernindet man ein Repository auf GitHub oder GitLab mit dem Provider und er übernimmt dann den Build und den Deploy Prozess. Aktuelle Beispiele für solche Anbieter sind _render.com_ und _netlify.com_.

Der klassische Weg ist, dass man eine eigene _pipeline_ bei seinem bevorzugten Soutce Code Management (SCM)-Provider definiert. Wir werden in diesem Modul gitLab dagür nutzen.



## Gitlab CI

> __Aufgabe AUTO 1__ (Leseauftrag)

Lest euch durch, was GitLab alles zum Thema Continuous Integration zu bieten hat.

https://docs.gitlab.com/ee/development/cicd/

---

> __Aufgabe AUTO 2__ (Einzelarbeit)

__TEST__

Erstellt eine Pipeline für euer GitLab Projekt und erstellt einen Test-Job, der unsere Tests durchführt.

__BUILD__

Erweitert eure Pipeline um einen Job, der unser Production-Image erzeugt und im Project-Repository unter dem Tag _latest_ speichert.

__DEPLOY__

folgendes Snippet sorgt dafür, dass unser Image auf dem Ziel-System ausgeführt wird.

```yaml
deploy-job:
    stage: deploy
    image: alpine
    before_script:
        # install envsubst and ssh-add
        - apk add gettext openssh-client
    script:
        # start ssh-agent and import ssh private key
        - eval `ssh-agent`
        - ssh-add <(echo "$SSH_PRIVATE_KEY")
        # add server to list of known hosts
        - mkdir -p ~/.ssh
        - chmod 700 ~/.ssh
        - touch ~/.ssh/known_hosts
        - chmod 600 ~/.ssh/known_hosts
        - echo $SSH_HOST_KEY >> ~/.ssh/known_hosts
        - echo "HOST *" > ~/.ssh/config
        - echo "StrictHostKeyChecking no" >> ~/.ssh/config
        # upload docker-compose to the server
        - scp compose.prod.yaml ec2-user@$DEPLOY_TARGET:/home/ec2-user/compose.yaml
        - ssh ec2-user@$DEPLOY_TARGET "cd /home/ec2-user; 
            docker login -u $CI_REGISTRY_USER 
                -p $CI_REGISTRY_PASSWORD $CI_REGISTRY;
            docker compose up -d"
    rules:
        # only deploy if new commit on main-branch
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

Die entsprechenden Variablen müssen in GitLab konfiguriert werden.

Geht dazu in die _project -> settings -> CI/CD_.
Unter _Variables_ müsst ihr folgende Variablen anlegen:

SSH_HOST_KEY:
Verbindet euch per SSH auf euren Server und ruft folgendes Kommando auf: 

```bash
sudo ssh-keygen -l -f ~/.ssh/authorized_keys
sudo ssh-keygen -l -f id_rsa.pub
```

Damit erhalten wir den Key, den wir auf unserem GitLab Worker registrieren, damit wir den Automationsfluss nicht durch eine manuelle Eingabe von _yes_ unterbrechen müssen.

SSH_PRIVATE_KEY:
Das ist der key, der bei der Erstellung der EC2 Instanz erzeugt wurde. GitLab benötigt diesen Key, damit die Verbindung zum Server klappt.

DEPLOY_TARGET:
Die IP Adresse oder der DNS-Name eures Servers, auf dem der MicroService deployed werden soll.

---

> __Aufgabe AUTO 3__ (Einzelarbeit)

Überprüft eure Pipeline, indem ihr ein Commit auf den _main_-Branch erzeugt.