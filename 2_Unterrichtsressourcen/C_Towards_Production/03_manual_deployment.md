# Deployment in der Cloud (von Hand)

Wir wollen nun endlich unseren Microservice öffentlich zur Verfügung stellen.
Dazu nutzen wir in diesem Kurs das Angebot von AWS-Academy.
Jeder sollte einen Account von AWS-Academy haben und zum entsprechenden Kur eingeladen worden sein.

## EC2 Instanz aufsetzen

Melde Dich hier als Student bei AWS-Academy an:
https://www.awsacademy.com/vforcesite/LMS_Login

Öffne den Kurs, zu dem Du eingeladen wurdest und öffne das Kursmodul : "_Launch AWS Academy Learner Lab_"

Du solltest diese Ansicht erhalten:

![aws_academy_lab](./x_gitressourcen/images/aws_academy_lab.png)

Falls das nicht funktioniert, stelle sicher, dass der Browser PopUps erlaubt.

Starte dann zuerst das Lab ("_> Start Lab_").

Sobald der rote Kreis neben "_AWS_" auf grün wechselt (kann eine Weile dauern), gelangst Du über diesen Link zur AWS Management Konsole.

---

Erstelle eine EC2 Instanz (_EC2 -> EC2-Dashboard -> Instance Starten_ ).
Das hier sind nur Empfehlungen. Experimentiere ruhig.

1. Betriebssystem: Amazon Linux als OS (__Amazon Linux 2023 AMI__)
1. Instance Typ: __t2.micro__ (ist ausreichend, eine t2.medium sollte auch für die Kursdauer gehen ($100))
1. Schlüsselpaar: neues Schlüsselpaar erstellen
    1. guten Namen einfallen lassen (z.B.: msvc-key)
    1. _ED25519_ und _.pem_ (SSH) auswählen. __unbedingt den generierten .pem-File speichern. Wir brauchen ihn unbedingt für die Verbindung zu unserem Server__
1. Netzwerkeinstellungen: _SSH_, _HTTP_, _HTTPS_ freischalten.
1. alles andere Standardeinstellungen
1. unter "erweiterte Details" - ganz unten bei "Benutzerdaten/User data" folgeendes Script einfügen:
    ```bash
    #! /bin/sh
    yum update -y
    yum install git -y
    yum install docker -y
    service docker start
    usermod -a -G docker ec2-user
    chkconfig docker on
    # install docker compose
    wget https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)
    mv docker-compose-$(uname -s)-$(uname -m) /usr/libexec/docker/cli-plugins/docker-compose
    chmod +x /usr/libexec/docker/cli-plugins/docker-compose
    systemctl enable docker.service --now
    usermod -a -G docker ec2-user
    ```
1. auf "_Instance Starten_" klicken

## Elastic IP Adresse erstellen

Damit wir nicht jedes mal, wenn die AWS-Academy Instanz pausiert wird unsere IP Adresse ändern müssen, holen wir uns eine Elastic IP von AWS.

1. EC2-Dashboard -> Elastic IP Adressen -> Elastic IP Adresse zuweisen
1. Alle Einstellungen auf Standard -> _Zuweisen_ klicken.
1. In der Übersicht auf die neue IP-Adresse klicken -> _"Elastic-IP-Adresse zuordnen"_ klicken
1. bei Instance unsere gerade erstellte EC2 Instanz auswählen.
1. auf "_Zuordnen_" klicken
1. Die IP Adresse (z.B.: 34.198.175.193) irgendwo notieren (z.B.: OneNote) - wir werden sie noch öfter brauchen.

## Verbindung per SSH herstellen

1. kopiere den .pem File (Private Key z.b.: _msvc-key.pem_) in das .ssh-Verzeichnis im Home-Verzeichnis Deiner WSL Ubuntu Distro.

1. wechsle in das .ssh Verzeichnis und tippe folgenden Befehl in die Konsole ein, damit der Schlüssel nicht öffentlich sichtbar ist.
    ```bash
    chmod 400 "lab-key.pem"
    ````
1. Mit SSH Verbinden
    1. Pfad zum private key als -i Parameter
    1. user ist ec2-user
    1. Elastic IP Adresse von vorhin verwenden

    ```bash
    ssh -i "~/.ssh/lab-key.pem" ec2-user@34.198.175.193
    ```
    1. kurz mit _yes_ bestätigen, dass der neue Host zur Liste der vertrauenswürdigen Server hinzugefügt wird.


## git clone aud EC2

Git konfigurieren und neuen Public Key erstellen:

```bash
ssh-keygen -t ed25519 -C [ihre@email.ch]
git config --global user.email [ihre@email.ch]
git config --global user.name [ihr gitlab_username]
git config --global --list
```

den neuen Public Key auf gitlab registrieren

mit

```bash
git clone git@gitlab.com:tbz-itcne23-msvc/blueprint-flask-prod.git
```

starten mit

```bash
docker compose -f compose.prod.yaml up --build
```

Test im Browser


