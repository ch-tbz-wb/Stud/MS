# Entwicklungsprozess mit GitLab-Flow

## Einleitung

Es macht Sinn, für jede Software-Entwicklung einen Prozess festzulegen, an den sich alle Entwickler halten müssen.

Damit wird verhindert, dass fehlerhafte Applikationen zum Kunden ausgeliefert werden.

Ungefähr 2010 hat sich ein Prozess entwickelt, der damals als revolutionär galt: [Git Flow](https://www.atlassian.com/de/git/tutorials/comparing-workflows/gitflow-workflow). Git Flow gilt heute als veraltet. Insbesondere deshalb, weil er zu komplex ist und sich nur schwierig in CI/CD Projekte integrieren lässt.

Heutzutage nutzt man sogenannte __trunk based__ Prozesse. Dabei werden für kleinere Änderungen eigene Branches erzeugt und dann so schnell wie möglich wieder auf den _default branch_ (bei GitLab = _main_) gepushed.

Wir werden uns GitLab Flow mal genauer anschauen:

## GitLab Flow

GitLab Flow ist ein trunk based Prozess, der sich gut in CI/CD einbinden lässt. Er ist einfacher als Git Flow und bietet dennoch eine gute Kontrolle über die Entwicklung.

Schaut euch mal diesen Link an:
https://about.gitlab.com/blog/2014/11/26/keeping-your-code-protected/

Der Prozess umfasst folgende Schritte:

1. **Entwicklung:** Alle Änderungen werden in einem eigenen Branch erstellt.
2. **Merge Request:** Sobald der Branch fertig ist, wird ein Merge Request erstellt.
3. **Review:** Der Merge Request wird von mindestens einem anderen Entwickler überprüft.
4. **Merge:** Wenn der Merge Request erfolgreich überprüft wurde, wird er in den main Branch gemerged.
5. **Deployment:** Der main Branch wird automatisch in die Produktionsumgebung deployt.

---

> __Aufgabe GIT_FLOW 1__ (Leseauftrag)

https://about.gitlab.com/topics/version-control/what-is-gitlab-flow/


```mermaid
---
title: GitLab Flow Diagram
---
gitGraph
   commit
   commit
   branch feature
   checkout feature
   commit
   commit
   checkout main
   merge feature
   commit
```

---

_Warum?_: wir wollen später nicht, dass das automatische deployment bei jeder kleinen Änderung (Commit) anspringt. Deshalb entwickeln wir auf einem extra Branch und können dort Tests ausführen. Erst wenn alles bereit ist, mergen wir den Entwicklungs-Zweig in den main Branch und stossen damit das automatische Deployment an.

> __Aufgabe GIT_FLOW 2__ (Pair-Programming)

Nutzt das Blueprint-Flask-Prod Template, und spielt im  Team den GitLab Prozess durch.

Erstellt einen neuen Branch _feature_.

in VSCode - oder mit dem Git-Befehl:

```bash
git checkout -b feature
```

wechselt ihr in den neuen Branch.

Ändert euren Code so, dass die Änderung im Server sichtbar wird.

Erstellt ein Commit auf dem neuen Branch.

Nutzt nun die _Merge Request_ Funktion auf GitLab, um einen offiziellen MR zu erstellen. Am Besten ihr fügt vorher euren Pair-Programming Partner als Developer zu eurem GitLab Repo hinzu.

Der Partner soll ein sorgfältiges Review durchführen und das entsprechend dokumentieren.

Merged den _feature-Branch_ in den Main Branch.





