# Testing

## Einführung

Wie jeder gute Pilot vor dem Start seines Flugzeugs Tests durchführt, sollten wir auch keine ungetestete Software auf die Benutzer "loslassen".

Wir werden deshalb unser Flask Template noch um eine Test-Umgebung erweitern.

> __Aufgabe TEST 1__ (Leseauftrag)

Lest folgende Links zum Thema Testing und Python durch.

- [Softwaretest (Wikipedia)](https://de.wikipedia.org/wiki/Softwaretest)
- https://flask.palletsprojects.com/en/3.0.x/testing/

Die folgenden Begriffe sollten danach keine Fremdwörter mehr sein:

- Qualitätsmanagement
- Teststufen
- Unit-Tests
- Fixtures

---

## Teil 1 - Testumgebung

als Basis dient wieder unser Flask Blueprint, den wir in Teil B erstellt haben. Natürlich könnt ihr die Übung auch mit eurem eigenen Flask-Service durchführen.

Hier der Link zum skalierbaren [Flask-Blueprint](https://gitlab.com/projects/55643521)


> __Aufgabe TEST 2__ (Pair-Programming)

erstellt eine Datei __Dockerfile.test__ im gleichen Verzeichnis, wie der existierende __Dockerfile__.

```yaml
# File: Dockerfile.test

# Base python package
FROM python:3.10.10-slim-buster

# Working directory
WORKDIR /app

# Copy the dependencies file
COPY requirements.txt requirements.txt

# Install the dependencies
RUN pip install -r requirements.txt

# Install Test Framework
RUN pip install pytest

# for flask web server
EXPOSE 5000

# add files
ADD . /app

# This is the runtime command for the container
# Add below line if not using docker-compose
CMD [ "python3", "-m" , "pytest"]
```

Mit diesem Dockerfile können wir einen Container mit unserem Microservice bauen. Wir verzichten hier auf Hot-Reload und kopieren deshalb unseren gesamten Code aus dem Entwicklungsverzeichnis in den Container.

Zusätzlich zu unseren Abhängigkeiten im File _requirements.txt_ installieren wir noch die neueste Version von __pytest__.

Wenn der Container startet, dann führt er automatisch das Test-Framework pytest aus.

---

Zusätzlich erstellen wir noch einen File __compose.test.yaml__.

```yaml
version: '3.9'

services:

  msvc-bp-prod-api:
    build:
      context: .
      dockerfile: 'Dockerfile.test'
    container_name: msvc-bp-test-api
```

In diesem docker compose file verzichten wir auf die mySQL Datenbank. Wir haben unseren Microservice so erstellt, dass falls keine DATABASE_URI angegeben wird, SQLite als Backup benutzt wird. Ausserdem haben wir das Port-Mapping rausgenommen, da wir nicht von aussen auf den Container zugreifen müssen.

---

erstellt nun im Verzeichnis _test_ folgende Datei:

```python

# File: __init__.py

import pytest
from app import create_app
from test.create_test_data import create_test_data

@pytest.fixture
def client():

    app = create_app()
    app.config.update({
        "TESTING": True,
    })

    with app.app_context():
        create_test_data()

    return app.test_client()

```

Das tolle ist hier, dass pytest unsere Flask App nutzt und daraus einen eigenen Kontext generiert. Jeder Test-Fall bekommt einen neuen App-Kontext und wird in der Fixture initialisiert.
In der Fixture können wir unser Test-Bed vorbereiten (z.B.: Testdaten in der Datenbank anlegen oder Umgebungsvariablen definieren).

> __Hinweis:__ Wir wollen ab sofort Test-Funktionalität und Microservice-Funktionalität strikt trennen. Deshalb müssen wir aus dem File ./app/__init__.py unbedingt alle Referenzen auf die Test-Umgebung entfernen.
>
> _Zeile 5_ ~~from test.create_test_data import create_test_data~~ löschen.
>
> _Zeile 25_ ~~create_test_data()  ## diese Zeile auskommentieren, wenn man keine Testdaten braucht~~ löschen. 
>
> Wenn man das nicht tut, dann funktioniert unsere normale Entwicklungsumgebung nicht mehr.

---

> __Aufgabe TEST 2__ (Pair-Programming)

Lasst euch unter folgendem Link inspirieren: https://codethechange.stanford.edu/guides/guide_flask_unit_testing.html

Erstellt eine Datei _test_student.py_ im _test_-Verzeichnis.

Testet nun den gesamten Student-Blueprint indem ihr nach folgendem Muster vorgeht:

```python
from test import client

def test_get_students(client):
    response = client.get("/students/")
    assert response.json[4]['name'] == 'Mega Tron'

def test_post_student(client):
    response = client.post("/students/", json={
            'name': 'Nina Hagen', 'level': 'AP'
        })
    assert response.status_code == 201

...

```

Die TestSuite wird so gestartet:

```bash
docker compose -f compose.test.yaml up --build
```

Danach sollte der Output so ähnlich aussehen:

```
Attaching to msvc-bp-test-api
msvc-bp-test-api  | ============================= test session starts ==============================
msvc-bp-test-api  | platform linux -- Python 3.10.10, pytest-8.1.1, pluggy-1.4.0
msvc-bp-test-api  | rootdir: /app
msvc-bp-test-api  | collected 7 items
msvc-bp-test-api  | 
msvc-bp-test-api  | test/test_root.py .                                                      [ 14%]
msvc-bp-test-api  | test/test_student.py ......                                              [100%]
msvc-bp-test-api  | 
msvc-bp-test-api  | ============================== 7 passed in 0.92s ===============================
msvc-bp-test-api exited with code 0
```

---
> __HINWEIS__ : es empfiehlt sich die Tests for jedem Einchecken durchzuführen.
Später werde wir sie in unsere CI/CD-Pipeline einbinden.

---

> __Aufgabe TEST 3__ (Pair-Programming)

Erstellt die Tests für den Courses-Blueprint 

## Wrap Up

- [x] Wir verstehen die Wichtigkeit von _Testing_ für die _Software-Qualität_.
- [x] Wir können mit Docker eine Testumgebung für _Unit-Tests_ erstellen.
- [x] Wir haben praktische Erfahrung mit _pytest_.
