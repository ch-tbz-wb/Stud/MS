# Flask production image

## Einführung

In unserer Entwicklungsphase des Microservices nutzen wir den integrierten Webserver von Flask.

Deshalb bekommen wir immer einen Warnhinweis, dass dieser Server auf keinen Fall für die Produktivumgebung eingesetzt werden soll.

```
msvc-bp-dev-api  |  * Serving Flask app 'app'
msvc-bp-dev-api  |  * Debug mode: on
msvc-bp-dev-api  | WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
msvc-bp-dev-api  |  * Running on all addresses (0.0.0.0)
msvc-bp-dev-api  |  * Running on http://127.0.0.1:5000
msvc-bp-dev-api  |  * Running on http://172.22.0.3:5000
msvc-bp-dev-api  | Press CTRL+C to quit
```

Wenn wir unseren Microservice auf einem öffentlich zugängigen Server laufen lassen wollen, dann brauchen wir einen richtigen WSGI Server dafür.

Wir werden dafür [Gunicorn](https://gunicorn.org/) verwenden.

## Dockerfile für Production

> __Aufgabe PROD 1__ (Pair-Programming)

Erstelle ein neues File _Dockerfile.prod_ im gleichen Verzeichnis wie das ursprüngliche _Dockerfile_, dass wir für die Entwicklung nutzen.

Ein guter Startpunkt ist hier:

```yaml
# File: Dockerfile.prod

# Base python package
FROM python:3.10.10-slim-buster

# Working directory
WORKDIR /app

# Copy the dependencies
COPY requirements.txt requirements.txt

# Install the dependencies
RUN pip install -r requirements.txt

# Install Gunicorn
RUN pip install gunicorn

# for flask web server
EXPOSE 5000

# add files
ADD . /app

# This is the runtime command for the container
# Add below line if not using docker-compose
CMD gunicorn -b 0.0.0.0:5000 wsgi:app

```

Auch wenn dieser Dockerfile sich nur minimal von unserem Entwicklungs-Dockerfile unterscheidet, sind hier wichtige Unterschiede:

- Zusätzlich zu unseren bisherigen Dependencies installieren wir _Gunicorn_. Wir geben hier keine Version an, damit wir immer den aktuellsten Gunicorn Webserver nutzen. Das hat den Vorteil, dass alle Security Patches genutzt werden. Hat aber auch den Nachteil, dass in Zukunft das Zusammenspiel mit unseren anderen Dependencies nicht mehr funktioniert.
- Als Command (_CMD__) starten wir nicht den Flask Server, sondern Gunicorn. Mit dem Parameter _-w 2_ sagen wir Gunicorn, dass er 2 Worker Nodes anlegen soll.

Damit Gunicorn unsere Flask App findet, brauchen wir noch einen zusätzlichen Python-File (_wsgi.py_) im gleichen Verzeichnis wie unser Dockerfile. Das ist notwendig, damit Gunicorn die app-Factory von Flask findet.

```python
# File: wsgi.py

from app import create_app

app = create_app()

if __name__ == '__main__':
    app.run()
```


## Docker Compose File für Production

> __Aufgabe PROD 2__ (Pair-Programming)

Wir legen einen zusätzlichen Compose File für die Production Umgebung an.


```yaml
# File: compose.prod.yaml

version: '3.9'

services:

  msvc-bp-prod-api:
    image: registry.gitlab.com/tbz-itcne23-msvc/blueprint-flask-prod:latest
    build:
      context: .
      dockerfile: 'Dockerfile.prod'
    container_name: msvc-bp-prod-api
    environment:
      - DATABASE_URI=mysql+mysqlconnector://root:root@msvc-bp-prod-db:3306/msvc-prod
    ports:
      - 5000:5000
    depends_on:
      msvc-bp-prod-db:
        condition: service_healthy

  msvc-bp-prod-db:
    image: mysql:5.7
    container_name: msvc-bp-prod-db
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: msvc-prod
    healthcheck:
      test: mysqladmin ping -h localhost -uroot --password=$$MYSQL_ROOT_PASSWORD
      start_period: 2s
      interval: 5s
      timeout: 5s
      retries: 55
    ports:
      - 3306:3306
    volumes:
      - msvc-prod-db:/var/lib/mysql

volumes:
  msvc-prod-db:
```

Mit dem Befehl

```bash
docker compose -f compose.prod.yaml up --build
```

Erzeugen wir das Production Image und starten es.

Es gibt einen kleinen Unterschied zum Entwicklungs-Compose File. Beim Service "msvc-bp.prod-api" haben wir einen Namen für das Image angegeben. Das wird später entscheidend, wenn wir die CI/CD-Pipeline aufbauen.

> __Hinweis:__ Wenn ihr einen Fork dieses Projekts erstellt habt, dann müsst ihr den Namen der Registry von "gitlab.com/tbz-itcne23-msvc" auf euren eigenen Namespace ändern.

---

> __Aufgabe PROD 3__  (Diskussion)

_Multi Dockerfile/compose.yaml_

Diskutiert, welche Vor- und Nachteile dieses Vorgehen (individuelles Dockerfile und compose.yaml) bei der Definition der Umgebungen _dev_, _test_ und _prod_ hat. Welche Alternativen gibt es?

---

> __Aufgabe PROD 4__  (Kurs-Challenge)

Das _Dockerfile_ für unseren Production-Microservice ist noch nicht optimiert. Obwohl als Base-Image schon eine Slim-Variante verwendet wird, ist das Image immer noch ca. 265 MB gross.

Wer schafft es, das kleinste Image für Production zu erzeugen?

Hier ein paar Hinweise:
- https://www.ecloudcontrol.com/best-practices-to-reduce-docker-images-size/
- https://www.docker.com/blog/how-to-use-the-alpine-docker-official-image/


## Wrap Up

- [x] Wir haben ein Image mit _Gunicorn_, das für die _Produktion_ benutzt werden kann.
- [x] Wir verstehen, wie das Design unseres Dockerfiles sich auf die Grösse unseres Images auswirkt.
- [x] Wir wissen, wie man den Image-Namen ändert, damit er mit einer Image-Registry auf GitLab verknüpft wird.

## Hidden gems

<details>
<summary>Zusätzliche Unterstützung</summary>

[hier nur klicken, wenn man es wirklich hart versucht hat](https://gitlab.com/projects/55748869)

</details>
