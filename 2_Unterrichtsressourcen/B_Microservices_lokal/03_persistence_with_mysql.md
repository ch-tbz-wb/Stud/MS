# Anbindung von Datenbanken

## CRUD

CRUD steht für

- _C_ reate
- _R_ ead
- _U_ pdate
- _D_ elete

und beschreibt damit alle Standardoperationen, die man für das Management von Daten in Datenbanken braucht.

Wenn wir also Datenbanken (Relational oder NoSQL) in unsere Microservices integrieren wollen, müssen wir uns mit diesem Thema auseinandersetzen.

> __Aufgabe SQL 1__ (Leseauftrag)

Arbeitet euch durch die folgenden Links, um ein Grundverständnis von CRUD und den Zusammenhang mit Rest zu bekommen.

- https://www.codecademy.com/article/what-is-crud
- https://mindsquare.de/knowhow/crud/

Beantwortet für euch die folgenden Fragen:

- Wie ist das Mapping zwischen CRUD und REST?
- Kann man CRUD-Aktionen standardmässig rückgängig machen?


## SQL Alchemy

https://www.sqlalchemy.org/ - ist ein Object Relational Mapper (ORM) für Python. Er wandelt Python Klassen in Datenbank-Tabellen um und umgekehrt.

> __Aufgabe SQL 2__ (Pair Programming)

Setzt euch zusammen und schreibt einen Microservice, der Studenten verwaltet.

Die Studenten werden in folgendem Format erfasst:

```plantuml

class Student {
    id : unique ID (Integer)
    name : full name (String)
    level : one of [HF, AP, PE, ICT] (String)
}
```

Mit dem Microservice sollen alle CRUD services abgehandelt werden können.

Hier findet ihr noch ein Quick-Start Tutorial: https://flask-sqlalchemy.palletsprojects.com/en/3.1.x/quickstart/


> Tip 1 : libriaries in requirements.txt

```bash
apiflask==2.3.2
mysql-connector-python==9.2.0
flask-sqlalchemy==3.1.1
```

> Tip 2 : imports in app.py

```python
from apiflask import APIFlask, Schema
from apiflask.fields import Integer, String
from apiflask.validators import Length, OneOf
from flask_sqlalchemy import SQLAlchemy
import os
```

> Tip 3 : connection string

Für den Anfang nutzen wir _SQlite_, die einfachste Art von Datenbank. SQLAlchemy verwendet dafür eine Datei, um die Tabellen und Daten zu speichern. Wir übergeben diese Information als config Parameter an unsere Flask app.

```python
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')
```

> Tip 4 : db-Objekt erstellen

Über dieses Objekt können wir auf unsere Datenbank zugreifen.

```python
db = SQLAlchemy(app)
```

> Tip 5 : Tabelle für Studenten

```python
class StudentModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32))
    level = db.Column(db.String(8))
```

> Tip 6 : Datenbank gemäss Schema erstellen

am Ende der app.py folgendes Snippet einfügen:

```python
with app.app_context():
    db.create_all()
```

> Tip 7 : Post Methode, um einen neuen Studenten zu erstellen

```python
@app.post('/students')
@app.input(StudentIn, location='json')
@app.output(StudentOut, status_code=201)
def create_student(json_data):
    student = StudentModel(**json_data)
    db.session.add(student)
    db.session.commit()
    return student
```

Ihr müsst zusätzlich zu dieser Route/View noch die views get_student, get_students, update_student und delete_student erstellen.

P.S.: StudentIn und StudentOut müssen noch definiert werden. Sie sollten die Klassen von der _Schema_-Klasse aus dem APIFlask Package ableiten. Es macht Sinn, bei StudentIn die ID des Studenten nicht als Parameter zu setzen, da diese ja von SQLAlchemy vergeben wird (_primary_key_)

Um die App zu testen, können wir wieder die interaktive Doku von APIflask nutzen (_localhost:5000/docs_).


## MySQL

Jetzt wechseln wir auf eine richtige Datenbank. Dafür müssen wir einen MySQL Container erstellen...

> __Aufgabe SQL 3__ (Pair Programming)

Erstellt einen mySQL Container und verbindet unsere Flask App damit.

> Tip 1: compose.yaml

```yaml
services:

  persistent-flask:
    container_name: persistent_flask_service
    build:
      context: .
    ports:
      - 5000:5000
    volumes:
      - .:/app
    environment:
      - DATABASE_URI=mysql+mysqlconnector://root:root@msvc-db:3306/msvc
      - CHOKIDAR_USEPOLLING=true
      - FLASK_DEBUG=1
      - FLASK_HOST=0.0.0.0
      - FLASK_PORT=5000
      - FLASK_APP=app
    depends_on:
      - msvc-db

  msvc-db:
    image: mysql:8.4.4
    container_name: msvc-db
    restart: always
    environment:
      MYSQL_DATABASE: msvc
      MYSQL_ROOT_PASSWORD: root
    ports:
      - 3306:3306
    volumes:
      - my-db:/var/lib/mysql

volumes:
  my-db:

```

... und unseren Connection String in der app.py anpassen.

> Tip 2 : mySQL connection String

wir holen uns den DB-Connection String aus den Environment Variablen, die wir im Docker-Compose File gesetzt haben.

```python
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URI')\
        or 'sqlite:///' + os.path.join(basedir, 'app.db')
```

_Bitte beachten:_ als Host geben wir im Connection String den Namen des Services der mySQL-DB an, den wir im _compose.yaml_ definiert haben ("_msvc-db_")

__Zusatzfrage:__ Was bewirkt das "_or_" in der app.config? Warum macht das im Docker Kontext Sinn?

---

> __Aufgabe SQL 4__ (Pair Programming)

Manchmal versucht unser Flask Server eine Verbindung mit der DB aufzubauen, obwohl diese noch nicht bereit ist. Das einfache _depends on_ im Compose-File reicht nicht aus, um das zu verhindern. Wir benötigen einen Health-Check.

Lest hier nach und erweitert unser Compose-File um einen Health-Check.

> Tip 3 : nützlicher [Link](https://dev.to/ku6ryo/frequently-used-2-healthcheck-recipes-for-docker-compose-dependency-resolution-2ad9) zum Thema _Health-Check_


---

Hier ein paar __Werkzeuge__, mit denen wir uns unsere Datenbank ansehen können:

- Sehr gutes Universaltool: https://dbeaver.io/
- MySQL Workbench verwalten: https://dev.mysql.com/downloads/workbench/

![Screenshot DBeaver](./x_gitressourcen/images/msvcDB_dbeaver.png)


## Exkurs PlantUML

vielleicht habt ihr schon einige Diagramme bemerkt, die hier im Repo verwendet werden. Damit man nicht ständig neue PNGs oder JPEGs generieren muss, kann man Diagramme in GitLab auch bequem per Text erstellen.

Dazu bietet sich PlantUML (https://plantuml.com/de/) an. Mit PlantUML kann man komplexe Klassen- und Sequenzdiagramme erstellen.
Besonders bei der Definition von Datenstrukturen in REST-Services ist das ein hilfreiches Werkzeug.

Damit man in Visual-Studio Code im Markdown Preview die PlantUML Diagramme sehen kann, muss man das Package "__Markdown Preview Enhanced__" installieren.

In den Settings der Erweiterung muss man dann noch bei "__Markdown-preview-enhanced: Plantuml Server__" einen funktionierenden PlantUML Server eintragen (z.B.: "https://kroki.io/plantuml/svg/").

---

> __Aufgabe SQL 5__ (Experiment)

Erstelle ein komplexes Klassendiagramm in PlantUML bei dem Students, Teacher und Courses abgebildet werden.

Hier noch die Sprachreferenz für PlantUML Klassendiagramme: https://plantuml.com/de/class-diagram 

Verständnisfragen: 
- Welchen Vorteil hat die Verwendung von Text-to-Diagram Technologien in der Software-Entwicklung?
- Welche Alternativen zu PlantUML gibt es?


## Komplexe Datenmodell in SQLAlchemy

> __Aufgabe SQL 6__ (Advanced)

Erweitere unsere Flask App mit der Möglichkeit Students in Courses zu registrieren und alle Courses anzuzeigen, die ein Student besucht.

> _Tip_: 
> ihr braucht dafür eine Hilfstabelle, die die Beziehung zwischen Students und Courses abbildet.
>
> _Zusatzinfo_:
> - [SQL Alchemy Docs Many to Many](https://docs.sqlalchemy.org/en/20/orm/basic_relationships.html#many-to-many)
> - [Einfaches Beispiel auf Medium](https://medium.com/@warrenzhang17/many-to-many-relationships-in-sqlalchemy-ba08f8e9ccf7)



## Wrap Up

- [x] Wir wissen was CRUD bedeutet und wie es mit REST zusammenhängt.
- [x] Wir können einfache Datenmodelle in einem ORM (SQLAlchemy) definieren und damit Daten in der SQL-Datenbank (mySQL) mit CRUD-Methoden verwalten.
- [x] Wir können mit Docker Compose eine Multi-Container Applikation erstellen.
- [x] Wir kennen Werkzeuge, mit denen man Datenbanken verwaltet (DBeaver).
- [x] Wir verstehen, wie man mit Code-2-Diagram Tools (plantUML) ein Daten-Design visualisiert und für die Dokumentation verwendet.


## Hidden gems

<details>
<summary>Zusätzliche Unterstützung</summary>

[hier nur klicken, wenn man es wirklich hart versucht hat](https://gitlab.com/tbz-itcne-msvc/example-flask-mysql)

</details>
