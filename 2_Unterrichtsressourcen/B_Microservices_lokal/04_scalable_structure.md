# Skalierbare Flask-Projektstruktur

Im Moment ist unsere gesamte Logik in einem einzigen File (_app.py_) untergebracht. Das ist gut für sehr kleine Microservices.
Je grösser unser Microservice jedoch wird, desto schwieriger und unübersichtlicher wird diese Methode.

Wenn wir uns die Datei app.py genauer anschauen, dann stellen wir folgende Struktur fest:

```mermaid

graph TB
    dep[Dependencies] --> conf[Configuration] --> mod[Models and Schemas] --> view[Routes and View-Functions]

```

Es liegt nahe, dass wir für jeden dieser Blöcke und für jede Ressource (z.B.: Student oder Course) einen eigenen Platz in unserer Projektstruktur finden..

Flask hat dafür eine Lösung: __Blueprints__

Blueprints helfen uns, unseren Microservice in kleinere, unabhängige Teile zu zerlegen.


> __Aufgabe SCALE 1__ (Leseauftrag)

Lest euch die die offizielle [Flask Doku](https://flask.palletsprojects.com/en/3.0.x/blueprints/) zum Thema Blueprints durch.

---


> __Aufgabe SCALE 2__ (Pair Programming)

Spielt folgendes Tutorial mit der mysql-Flask-App aus dem Datenbank-Teil durch. 

_Tutorial_: [Digital Ocean - how to structure a large Flask application](https://www.digitalocean.com/community/tutorials/how-to-structure-a-large-flask-application-with-flask-blueprints-and-flask-sqlalchemy)

Erstellt dafür in GitLab einen [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) von  dem GitLab-repo [example-flask-mysql](https://gitlab.com/tbz-itcne23-msvc/example-flask-mysql)

> Ignoriert alle Templates! (wir brauchen kein Web-UI)

> ENV Variablen setzen wir in der compose.yaml

> Verwendet Docker anstatt venv!


> Tip: https://stackoverflow.com/questions/3357825/python-sqlalchemy-how-to-relate-tables-from-different-modules-or-files


Unsere Verzeichnisstruktur sollte am Ende so aussehen:

```bash
.
├── Dockerfile
├── README.md
├── app
│   ├── __init__.py
│   ├── courses
│   │   ├── __init__.py
│   │   └── routes.py
│   ├── extensions.py
│   ├── models
│   │   ├── course.py
│   │   ├── registration.py
│   │   ├── student.py
│   │   └── test_data.py
│   └── students
│       ├── __init__.py
│       └── routes.py
├── compose.yaml
├── config.py
└── requirements.txt
```

Unsere ursprünglich app.py wandeln wir in ein Python Package um.

_Info_ - mit einer Datei 

```
__init__.py
```

in einem Verzeichnis sagen wir dem Python Interpreter, dass dieses Verzeichnis Python-Package ist. Damit können wir Klassen aus diesem Package mit z.B.:

```python
from app.models.course import Course
```

in unsere Applikation einbinden.


## Wrap Up

- [x] Wir wissen, wie man _Python-Packages_ erstellt.
- [x] Wir verstehen das Prinzip von _Flask-Blueprints_ und können einen komplexen Microservice in kleine Einheiten aufteilen.


## Hidden gems

<details>
<summary>Zusätzliche Unterstützung</summary>

[hier nur klicken, wenn man es wirklich hart versucht hat](https://gitlab.com/projects/55643521)

</details>
