# B - Microservices lokal entwickeln

[[_TOC_]]

Bevor wir Microservices in der cloud betreiben, fangen wir mit lokalen Entwicklungs-Versionen an. Wir führen docker ein und verwenden flask, ume eine REST API zu erstellen.


## Kompetenzen

- B10.1 Die Architektur der Software bestimmen und die Entwicklung unter Berücksichtigung von Betrieb und Wartung planen und dokumentieren (Niveau: 3)

## Minimal Flask

Unter Verwendung von Flask wird ein minimales Gerüst für einen Microservice erstellt und mit docker in eine Container-Laufzeit gebracht.

### Hands on

[Minimal Flask](./01_minimal_dockerized_flask.md)

## Rest (synchrone Kommunikation)

REST APIs sind der Schlüssel für die synchrone Kommunikation zwischen Microservices. Es ist deshalb wichtig zu verstehen, wie man gute APIs entwirft, umsetzt und testet.

### Hands on

[REST APIs](./02_restful_api_postman.md)

## Persistenz

Wir schliessen eine Datenbank an unseren Microservice an, damit wir Daten auch bei Beendigung eines Services noch verfügbar haben.

### Hands on

[Persistenz mit mySQL](./03_persistence_with_mysql.md)


## Skalierbare Projektstruktur

Damit wir unsere Microservices einfach erweitern können und eventuell später einzelne Teile einfacher herausschneiden können, um daraus neue Mircoservices zu machen, müssen wir unsere Struktur verbessern.  

### Hands on

[Skalierbare Struktur](./04_scalable_structure.md)




## Links

*Allgemeine Links welche sich nicht in die Unterthemen einordnen lassen*

