# RESTful API Design

[[_TOC_]]

## Einführung REST

```plantuml
skinparam monochrome true
skinparam packageStyle rectangle
hide circle
hide empty members

class "Client" as client 
class "Server (API)" as api
class "Data Storage" as db

client "request (HTTP or HTTPS)" --> api 
api  --> "response (JSON)" client
api --> "query" db
db "data" --> api 

```

Der Standard für die Kommunikation mit und zwischen Microservices ist REST. REST steht für __representational state transfer__. 
Diese Methode überträgt den aktuellen Zustand des Servers über das HTTP-Protokoll zum Client. Eine REST Schnittstelle benutzt __URIs__, um Ressourcen zu adressieren und die HTTP Methoden GET, PUT, POST und DELETE, um diese zuzugreifen.

Lese dir folgende Links durch und verschaffe dir einen Überblick zum Thema.

- https://de.m.wikipedia.org/wiki/Representational_State_Transfer
- https://aws.amazon.com/de/what-is/restful-api/
- https://www.computerweekly.com/de/definition/RESTful-API


## Flask und REST

> __Aufgabe REST 1__ (Einzelarbeit)

Flask unterstützt REST von Natur aus. Allerdings müssen wir unsere Routen entsprechend aufbauen.

Informiere dich unter diesen Links und baue die minimal-Flask-App mit REST-konformen Routen aus.

- https://auth0.com/blog/best-practices-for-flask-api-development/

 
> __Aufgabe REST 2__ (pair programming)

Hier ein kleines Code Beispiel für einen RESTful Taschenrechner.
Vervollständige es und baue es als neue route in unsere minimal-Flask-App ein.

```python
# Hole die Parameter a und b aus der Anfrage
a = request.args.get("a", type=float)
b = request.args.get("b", type=float)
# Prüfe, ob die Parameter gültig sind
if a is None or b is None:
    return jsonify({"error": "Invalid parameters"})
# Führe die Rechenoperation aus
if op == "add":
    result = a + b
elif op == "sub":
    result = a - b
elif op == "mul":
    result = a * b
elif op == "div":
    result = a / b
else:
    return jsonify({"error": "Invalid operation"})
# Gib das Ergebnis als JSON zurück
return jsonify({"result": result})
```

> Hinweis 1: die Route könnte z.B. mit:
>```
>@app.route("/calc/<op>")
>```
> definiert werden.

> Hinweis 2: zum aufrufen des Rechners kann man z.B.: folgende URL benutzen:
>```
>http://localhost:5000/calc/mul?a=2.3&b=4.0
>```

## Postman

Postman ist ein hervorragendes Werkzeug für den Entwurf und das Testen von REST APIs. Wir werden nun unseren minimal-Flask-App mit postman testen.

> __Aufgabe REST 3__ (Einzelarbeit)

Installiere postman (https://www.postman.com/downloads/) und lege einen postman-Account an. 
Sende von postman ein HTTP request an die minimal-Flask-App.

Tipp: nicht vergessen, die BaseURL in Postman auf __localhost:5000__ zu setzen.

Experimentiere mit den verschiedenen Möglichkeiten von postman:
- Collections
- Web Client
- GitLab Integration (für Fortgeschritten)

Vertiefende Links:
- https://dev.to/terieyenike/creating-apis-with-flask-and-testing-in-postman-2ojn


## OpenAPI und APIFlask

Für die Spezifikation von APIs hat sich in den letzten Jahren der Standard OpenAPI (https://www.openapis.org) aus dem älteren Swagger entwickelt.

Postman kann OpenAPI Spezifikationen laden und weiterverarbeiten. Es ist jedoch recht mühsam eine API Spezifikation von Grund auf neu zu schreiben.
Deshalb verwenden wir meistens einen Code-First Ansatz.

> __Aufgabe REST 4__ (pair programming)

Schaut euch zusammen APIFlask (https://apiflask.com/api-docs/) an.

Nutze die Anleitung unter https://apiflask.com/migrations/flask/, um unsere minimal-Flask-App mit APIFlask zu erweitern.

Nutzt @app.input und @app.output, um die Input und Output Parameter der API zu beschreiben.

Schaut euch unter localhost:5000/docs die interaktive API Dokumentation an und experimentiert damit.


## Externe APIs verwenden

Um unsere App zu bereichern, können wir andere, öffentliche APIs benutzen.

> __Aufgabe REST 5__ (pair programming)

Erweitert unsere minimal-Flask-App um eine Route, die einen Betrag in EUR anhand des aktuellen BitCoin-Kurses in BitCoins umrechnet.

__Tipp:__ ihr braucht das __requests__-Package (in die requirements.txt einfügen) und folgende externe URL: 

```
https://data-api.coindesk.com/index/cc/v1/latest/tick?market=cadli&instruments=BTC-USD
```

Hier ist eine Liste mit APIs, die keine Authentifizierung erfordern: https://apipheny.io/free-api/

Erstellt weitere Routen mit öffentlichen APIs (sonderbarerweise ist das Thema _Katzen_ und _Hunde_ sehr prominent)


## Hidden gems

<details>
<summary>Zusätzliche Unterstützung</summary>

[hier nur klicken, wenn man es wirklich hart versucht hat](https://gitlab.com/tbz-itcne-msvc/example-flask-minimal)

</details>

## Bonus Material

<details>
<summary>für alle, die das hier schon erledigt haben, hier eine kleine Zusatz-Challenge:</summary>

Deep Seek ist gerade in aller Munde. Wollt ihr mal euren eigenen Deep Seek Container aufsetzen?

Versucht doch mal Folgendes:

```bash

docker run -d -v ollama:/root/.ollama -p 11434:11434 --name ollama ollama/ollama
docker exec -it ollama ollama run deepseek-r1:1.5b
```

Damit erhaltet ihr einen MicroService mit dem kleinsten DeepSeek-LLM über Ollama auf Port 11434.

Schaut euch die API an und experimentiert:
https://github.com/ollama/ollama/blob/main/docs/api.md


</details>


