# Minimal Flask Server

In diesem Walkthrough erstellen wir unseren ersten Flask Microservice Container. Der entstehende Container ist für die Entwicklung (und nicht für den produktiven Betrieb!!!) gedacht.

Wir werden diesen Service später nochmal neu aufsetzen, um verschiedene Szenarien abzubilden und eine ordentliche Erweiterbarkeit zu gewährleisten. Aber um Flask einmal kurz zu erleben, reicht dieses Beispiel aus.

[[_TOC_]]

## Flask Grundgerüst


### erstelle eine Datei app.py mit folgendem Inhalt:

```python
# file: app.py

from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello ITCNE!'
```
kurz erklärt:
1. wir importieren die flask Bibliothek
2. wir erstellen ein Flask Objekt für die spätere Referenz
3. wir definieren unsere Standard Route, die später veröffentlicht wird.

### Erstelle eine Datei requirements.txt

Damit Python weiss, welche Abhängigkeiten unseres Services zu Bibliotheken bestehen, geben wir in dieser Datei die benötigten Bibliotheken für PIP (den Python Paket-Manager) an.
**Stichwort: Version Pinning**

```python
# file: requirements.txt

Flask==3.1.0

```

## Ein docker image erstellen

Erstelle ein Dockerfile mit folgendem Inhalt:

```Dockerfile
# file: Dockerfile

# Base python package
FROM python:3.13.2

# Working directory
WORKDIR /app

# Copy the dependencies
COPY requirements.txt requirements.txt

# Install the dependencies
RUN pip install -r requirements.txt

COPY . .

# Executable commands
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]

```

Unser Image baut auf einem Standard-Python Image auf. Damit sparen wir uns die manuelle Installation von Python und den entsprechenden Tools.


Zusatzinfo: [Unterschied CMD und Entrypoint](https://stackoverflow.com/questions/52063964/what-should-i-put-for-docker-cmd-and-entrypoint-for-flask-app-running-python-my)


## Docker Image bauen und ausführen

führe im Terminal folgendes Kommando aus (Punkt am Ende nicht vergessen!):

```bash
docker build -t minimal-flask .
```

Damit erstellen wir ein Image mit Python und unserem minimalen Service.

Mit diesem Befehl im Terminal starten wir unser Image in einem Container und öffnen Port 5000.
Mit *-d* "detachen" wir unser Terminal vom Container, damit er im Hintergrund läuft. 

```bash
docker run -d -p 5000:5000 --name minimal-flask-service minimal-flask
```
---

> **Hinweis:** auf MacOs ist der Port 5000 meistens schon belegt. In diesem Fall können wir ein anderes Port mapping verwenden. Anstatt *-p 5000:5000* verwenden wir einfach *-p 5001:5000*. Damit haben wir den Flask Server auf Port 5000 aus unserem Container auf den Port 5001 ausserhalb des Containers "umgeroutet".

Wir können überprüfen, ob unser Service läuft:

```bash
docker ps
```


## Test mit curl

Damit wir sehen, ob unser Service funktioniert, testen wir mit curl:

```bash
curl 127.0.0.1:5000
```

Es sollte die Ausgabe im Terminal erscheinen, die wir bei der Standard-Route unseres Flask Servers definiert haben (Hello ITCNE!).

Alternativ können wir die Adresse auch im Web-Browser laden. 

Der Service wir mit:

```bash
docker stop minimal-flask-service
```

wieder beendet.


damit wir nicht jedesmal alle Parameter im docker run Befehl übergeben müssen, können wir noch eine andere Methode ausprobieren. Wir verwenden Docker Compose, um den Container mit allen notwendigen Parametern zu starten.

Damit wir den Port 5000 wieder frei bekommen, löschen wir den alten Container mit:

```bash
docker rm minimal-flask-service
```


## docker compose und hot reload

Füge eine Datei __compose.yaml__ zur minimalen flask app hinzu. 
Unsere File Struktur sollte im Moment so aussehen:

```
.
├── Dockerfile
├── app.py
├── compose.yaml
└── requirements.txt
```

Kopiere folgenden Code in das compose.yaml file:

```yaml

services:
  minimal-flask:
    container_name: minimal_flask_service
    build:
      context: .
    ports:
      - 5000:5000
    environment:
      - CHOKIDAR_USEPOLLING=true
      - FLASK_DEBUG=1
      - FLASK_HOST=0.0.0.0
      - FLASK_PORT=5000
      - FLASK_APP=app
    volumes:
      - .:/app
 
    
```

kurz erklärt:
- wir definieren einen Service __minimal-flask__
- den zu benutzenden Container nennen wir __minimal_flask-service__
- mit _build_ erklären wir docker compose, dass unser Dockerfile für den Container im aktuellen Verzeichnis liegt.
- wir öffnen __port 5000__ auf dem Container und mappen ihn auf __port 5000__ auf dem Host.
- wir legen einige Umgebungsvariablen fest, die später in die __ENV__ des Containers wandern. Damit überschreiben wir übrigens Umgebungsvariablen im Dockerfile.
- __Wichtig:__ in der _volumes_ Sektion definieren wir einen __bind mounts__ zwischen dem aktuellen Verzeichnis auf dem Host und dem Verzeichnis _/app_ im Container.

> es gibt in docker drei Arten von volumes: 
> 1. _bind mount_ : mapping auf das Host File System - benötigt für hot reload
> 2. _docker volume_ : ein Laufwerk, dass von der docker engine verwaltet wird
> 3. _external volume_ : ein Laufwerk, dass vom user verwaltet wird (kann zum Beispiel ein Netzlaufwerk sein)

> es empfiehlt sich, dass Thema docker volumes nochmal hier nachzulesen: https://docs.docker.com/storage/volumes/


## HotReload Test

für die Entwicklung ist _hot reload_ extrem hilfreich, da wir bei Änderungen unseres Source Codes nicht jedes mal den Container neu bauen müssen.

Teste, ob hot reload funktioniert, indem du den flask service nun mit

```bash
docker compose up
```

startest. Wenn Du etwas in der Datei __app.py__ änderst, sollte die Änderung nach einem Browser refresh sofort sichtbar sein.



## weiterführende Links
- https://medium.com/@rokinmaharjan/running-a-flask-application-in-docker-80191791e143
- https://flask.palletsprojects.com/en/2.3.x/quickstart/


