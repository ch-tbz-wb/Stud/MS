# Einführung Microservices

[[_TOC_]]

Warum Microservices so ein grosses Thema sind.

## Kompetenzen
- A2.2 Das Interesse von Adressaten gewinnen und glaubwürdig sowie überzeugend kommunizieren (Niveau: 3)
- B6.3 Den Informationsbedarf für Entscheidungssituationen aufgrund von Anspruchsgruppen bestimmen (Niveau: 2)
- B10.1 Die Architektur der Software bestimmen und die Entwicklung unter Berücksichtigung von Betrieb und Wartung planen und dokumentieren (Niveau: 3)


## Lernziele / Taxonomie 

- versteht warum es Microservices braucht, was Microservices sind und wo sie eingesetzt werden können.

## Hands-on
- [Warum Microservices](01_warum_microservices.md)
- [Gruppenarbeit - MS in der Praxis](./02_GRP_microservices_in_der_praxis.md)
- [MS im Detail](./03_microservices_im_detail.md)
- [Technologie Stack](./04_technology_stack.md)

## Links

*Allgemeine Links welche sich nicht in die Unterthemen einordnen lassen*
