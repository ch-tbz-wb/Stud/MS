# Microservices in der Praxis

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Gruppenarbeit |
| Aufgabenstellung  | Firmen finden, die Microservices verwenden  |
| Zeitbudget  |  1 Lektion |
| Ziel | Kennt reale Bedürfnisse für den Einsatz von Microservices |

---

## Phase 1 - Marktanalyse

*Aufgabe* - Die gesamte Klasse sammelt Namen von grossen Organisationen, die in ihren Produkten eine Microservice-Architektur einsetzen.

Die Namen werden von der Lehrperson gesammelt und gruppiert.

Zeit: 10 Minuten

Anhaltspunkte:
- https://de.m.wikipedia.org/wiki/Microservices


## Phase 2 - Analyse der Anwendungsfälle

*Ziel* - Selbst herausfinden, wo Microservice-Architekturen in der Praxis eingesetzt werden und warum.

*Aufgabe* - Teilt euch in 3er oder 4er Teams ein. Jedem Team wird eine Organisation und ein Produkt aus Phase 1 zugewiesen.

Recherchiert im Netz folgende Aspekte zu eurem Produkt:
- Welche Art von Services bietet das Produkt den Endbenutzern? (z.B.: Video Streaming)
- Welche Anforderungen haben die Services? (z.B.: hohe Verfügbarkeit)
- Wie unterstützt die Microservice-Architektur die jeweiligen Anforderungen?
- Welche Alternativen in der Architektur hätten die Organisationen und welche Nachteile würden sich daraus ergeben?

Dokumentiert die Erkenntnisse in einer PPT.

Zeit: 30 Minuten

## Phase 3 - Präsentation und Diskussion der Ergebnisse

Jede Gruppe erhält 5 Minuten Zeit, um ihr  zugewiesenes Unternehmen und ihre Anwendungsfälle vorzustellen. Im Anschluss werden die Ergebnisse diskutiert.

Zeit: abhängig von der Anzahl Gruppen

## Phase 4 - Grenzen von Microservices

Bitte schaut euch in der gleichen Gruppenkontellation wie in Phase 2 und 3 folgenden Link an:

https://www.heise.de/meinung/Amazons-neuer-Modulith-Services-neu-zuschneiden-macht-noch-keinen-Monolithen-8990271.html

versucht noch weitere Quellen zu finden, die eine Abkehr von Microservices vorschlagen. 

Identifiziert Anwendungsfälle und Rahmenbedingungen, bei denen der Einsatz von Microservices nicht sinnvoll ist.