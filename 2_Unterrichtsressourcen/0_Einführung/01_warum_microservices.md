# Warum Microservices?

## Warum "warum"?

Es macht immer Sinn, mit dem *"Warum"* zu starten, bevor man sich über das *"Wie"* und das *"Was"* Gedanken macht. Besonders in der IT kann man so vermeiden, dass man sich zu sehr auf Technologie-Diskussionen einlässt und mehr in die Frage investiert: *"Welches Problem wollen wir eigentlich lösen?"*.

<img src="./x_gitressourcen/images/golden_circle.png" width="50%">

Quelle: https://simonsinek.com/


## Exponentielle Komplexität

Moderne IT Applikationen benötigen eine Unmenge an verschiedenen Bibliotheken, Komponenten, Skripts etc.
Dabei entwickelt sich die Komplexität der Anwendungen exponentiell und damit auch der Aufwand für Wartung und Weiterentwicklung. 

Sah die Welt im Jahre 1956 noch so aus (**5 MByte** Festplatte)...


<img src="./x_gitressourcen/images/5MB_HDD_1956.jpg" width="60%">


Liegt die Komplexität im Jahre 2024 in einer anderen Dimension:

<img src="./x_gitressourcen/images/IT_complexity_stack_overflow.png" width="90%">

Quelle: https://www.outsystems.com/blog/posts/manage-it-complexity/

Wir müssen uns also eine Strategie einfallen lassen, um die steigende Komplexität in den Griff zu bekommen.

## Wie isst man einen Elefanten?

<img src="./x_gitressourcen/images/How_to_eat_an_elephant_DALL_E_2024.jpg" alt="wie isst man einen Elefanten?" width="70%">

**Antwort: Biss für Biss.**

# Teile und Herrsche

"Teile und herrsche" ist ein sehr altes Prinzip aus der Politik. 
https://de-academic.com/dic.nsf/dewiki/340535

Microservices helfen uns genau dabei. Dadurch, dass jede Applikation in ihre Hauptaspekte unterteilt wird, können wir für jeden Aspekt einen eigenen, passenden, minimalen Technologiestack aufbauen. Für die Weiterentwicklung und Wartung des Aspekts können wir dann ein dediziertes Team einsetzen.

Moderne Container-Technologien (wie z.B.: Docker/Podman etc.) unterstützen uns dabei.

