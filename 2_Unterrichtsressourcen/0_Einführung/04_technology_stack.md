# Technology Map

Hier ist eine Übersicht, der Technologien, die wir in diesem Modul benutzen werden. Für jede der Technologien gibt es Alternativen, auf die wir jeweils kurz eingehen werden.

## Application

```plantuml

skinparam monochrome true
skinparam packageStyle rectangle
hide circle
hide empty members


class Python {
    <img:https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/438px-Python-logo-notext.svg.png{scale=0.15}>
}

class Flask {
    <img:https://flask.palletsprojects.com/en/stable/_images/flask-horizontal.png{scale=0.2}>
}

class AWS {
    <img:https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Amazon_Web_Services_Logo.svg/640px-Amazon_Web_Services_Logo.svg.png{scale=0.1}>
}


class DockerCompose {
    <img:https://media.licdn.com/dms/image/C4D12AQGpN-zpU0zFog/article-cover_image-shrink_600_2000/0/1590393863972?e=2147483647&v=beta&t=luPHO7Ijz8Gg5lRGdynQV9UfcSUDRZS30km9_tJnlXY{scale=0.3}>
}


class MySQL {
    <img:https://upload.wikimedia.org/wikipedia/de/thumb/d/dd/MySQL_logo.svg/640px-MySQL_logo.svg.png{scale=0.15}>
}

class Redis {
    <img:https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Logo-redis.svg/640px-Logo-redis.svg.png{scale=0.15}>
}

class Gunicorn {
    <img:https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Gunicorn_logo_2010.svg/640px-Gunicorn_logo_2010.svg.png{scale=0.2}>
}


Gunicorn -- Python
Flask -- Python
DockerCompose - MySQL
AWS -- DockerCompose
DockerCompose -- Gunicorn
DockerCompose -left Redis
Flask - Gunicorn


```

### Python
Python ist eine universelle, üblicherweise interpretierte, höhere Programmiersprache. Sie hat den Anspruch, einen gut lesbaren, knappen Programmierstil zu fördern. So werden beispielsweise Blöcke nicht durch geschweifte Klammern, sondern durch Einrückungen strukturiert.
https://www.python.org/

### Docker Compose
Docker Compose ist ein Tool zur Definition und Ausführung von Multi-Container-Anwendungen. Es ist der Schlüssel für eine optimierte und effiziente Entwicklung und Bereitstellung.
https://docs.docker.com/compose/

### Flask
Flask ist ein in Python geschriebenes Webframework. Sein Fokus liegt auf Erweiterbarkeit und guter Dokumentation. Die einzigen Softwareabhängigkeiten (engl. dependency) sind Jinja2, eine Template-Engine, und Werkzeug, eine Softwarebibliothek zum Erstellen von WSGI-Anwendungen.
https://flask.palletsprojects.com/en/3.0.x/

### Gunicorn
Gunicorn (Green Unicorn) ist ein Web Server Gateway Interface (WSGI) HTTP-Server. Der Server ist mit vielen Webframeworks kompatibel und benötigt wenig Ressourcen.
https://gunicorn.org/

### Redis
Redis ist eine In-Memory-Datenbank mit einer einfachen Schlüssel-Werte-Datenstruktur (englisch key value store) und gehört zur Familie der NoSQL-Datenbanken (ist also nicht relational). Redis ist Open Source und laut einer Erhebung von DB-Engines.com der verbreitetste Schlüssel-Werte-Speicher.
https://redis.io/

### MySQL
MySQL ist eines der weltweit verbreitetsten relationalen Datenbankverwaltungssysteme. Es ist als Open-Source-Software sowie als proprietäre Enterpriseversion für verschiedene Betriebssysteme verfügbar und bildet die Grundlage für viele dynamische Webauftritte. 
https://www.mysql.com/de/

### Amazon Web Services (AWS)
Amazon Web Services (AWS) ist ein US-amerikanischer Cloud-Computing-Anbieter, der 2006 als Tochterunternehmen des Online-Versandhändlers Amazon.com gegründet wurde. Zahlreiche populäre Dienste wie beispielsweise Dropbox, Netflix, Foursquare oder Reddit greifen auf die Dienste von Amazon Web Services zurück. 2017 stufte Gartner AWS als führenden internationalen Anbieter im Cloud Computing ein.
https://aws.amazon.com/de/

## Development



```plantuml

skinparam monochrome true
skinparam packageStyle rectangle
hide circle
hide empty members


class DockerDesktop {
    <img:https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Docker_%28container_engine%29_logo.svg/640px-Docker_%28container_engine%29_logo.svg.png{scale=0.2}>
}

class Ubuntu {
    <img:https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Logo-ubuntu_cof-orange-hex.svg/480px-Logo-ubuntu_cof-orange-hex.svg.png{scale=0.1}>
}

class VSCode {
    <img:https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/240px-Visual_Studio_Code_1.35_icon.svg.png{scale=0.2}>
}

class GitLab {
    <img:https://images.ctfassets.net/xz1dnu24egyd/1hnQd13UBU7n5V0RsJcbP3/769692e40a6d528e334b84f079c1f577/gitlab-logo-100.png{scale=0.15}>
}

class Postman {
    <img:https://upload.wikimedia.org/wikipedia/commons/c/c2/Postman_%28software%29.png{scale=0.15}>
}


DockerDesktop -- VSCode
Ubuntu -- VSCode
GitLab -- VSCode
VSCode -- Postman


```

### VS Code
Visual Studio Code (kurz VS Code) ist ein kostenloser Quelltext-Editor von Microsoft. Visual Studio Code ist plattformübergreifend für die Betriebssysteme Windows, macOS und Linux verfügbar. Visual Studio Code wird hauptsächlich von einem Team in der Schweiz entwickelt, das von Erich Gamma geleitet wird.
https://code.visualstudio.com/

### Ubuntu
Ubuntu Linux, ist eine GNU/Linux-Distribution, die auf Debian basiert. Der Name Ubuntu bedeutet auf Zulu etwa „Menschlichkeit“. Die Entwickler verfolgen mit dem System das Ziel, ein einfach zu installierendes und leicht zu bedienendes Betriebssystem mit aufeinander abgestimmter Software zu schaffen. Die Zahl der Nutzer von Ubuntu wurde 2016 auf etwa 25 Millionen geschätzt.
https://ubuntu.com/

### Docker Desktop
Docker Desktop ist eine sichere, sofort einsatzbereite Containerisierungssoftware, die Entwicklern und Teams ein robustes, hybrides Toolkit zum Erstellen, Freigeben und Ausführen von Anwendungen an jedem Ort bietet.
https://www.docker.com/products/docker-desktop/

### Postman
Postman ist eine API-Plattform für die Erstellung und Nutzung von APIs.
https://www.postman.com/