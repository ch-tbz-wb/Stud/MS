# Microservices im Detail

## Monolith

In der Vergangenheit war es oft üblich, komplexe Applikationen als einen grossen Block (dem Monolith) zu betreiben.

Viele Applikationen fangen klein an und werden dann im Laufe der Zeit erweitert.

```plantuml

skinparam monochrome true
skinparam packageStyle rectangle
hide circle
hide empty members


package SocialService {
    class Users
    class Threads
    class Posts
    Users-[hidden]Threads
    Threads-[hidden]Posts
}

```

Bei diesem Ansatz entstehen einige Probleme:

- Die *Wartbarkeit* der Anwendung wird immer schwieriger, da die einzelnen Funktionen nicht unabhängig voneinander sind.
- Die *Skalierbarkeit* der Anwendung ist nur schwer zu erreichen, da die einzelnen Funktionen sich nicht unabhängig auf mehr Rechenpower verteilen können.
- Die *Weiterentwicklung* eines Monolithen ist deutlich schwerer, da die Funktionen oft von mehreren Teams entwickelt werden.

## Microservices

Microservices sind keine Technologie sondern ein *Architekturmuster* (Pattern). Die prinzipielle Idee ist es, einen Monolithen in mehrere, unabhängige Applikationen zu zerlegen. Diese **Services** können dann miteinander Daten austauschen und gemeinsam einen grösseren Zweck erfüllen.

```plantuml

skinparam monochrome true
skinparam packageStyle rectangle
hide circle
hide empty members

package UsersService {
    class Users
}

Package ThreadsService {
    class Threads
}

Package PostsService {
    class Posts
}

UsersService-[hidden]ThreadsService
ThreadsService-[hidden]PostsService


```

Bei dieser Architektur können die einzelnen Services sogar vollkommen andere Technologie-Stacks verwenden.

## Weiterführende Links

- [Microservices Characteristics AWS]
(https://aws.amazon.com/de/microservices/#:~:text=Characteristics%20of%20Microservices&text=Each%20component%20service%20in%20a,or%20implementation%20with%20other%20services.)
- [12 Faktoren für gutes Microservice Design](https://12factor.net/de/)

 

