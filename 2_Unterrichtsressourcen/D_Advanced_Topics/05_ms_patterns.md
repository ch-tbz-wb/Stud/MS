# Microservice Patterns

auf der Seite microservices.io gibt es sehr viel Material zum Thema Microservices.
Der Autor ist Chris Richardson, der Bücher und Vorträge zum Thema Microservices veröffentlicht.

Auf [dieser Übersichtskarte](https://microservices.io/patterns/index.html) sind allerrelevanten Architektur-Muster zum Thema MIcroservices dargestellt.

---

> __Aufgabe MPAT 1__ (Einzelarbeit)

Nimm Dir 20 Minuten Zeit, und verschaffe Dir einen Überblick über die Patterns.

Gehe auf dieses [Whiteboard](https://teams.microsoft.com/l/entity/95de633a-083e-42f5-b444-a4295d8e9314/_djb2_msteams_prefix_2322870605?context=%7B%22channelId%22%3A%2219%3AF6JhZoyZdn8_XROVWovrTx2ibB1ECs99UH-tuRtGwPc1%40thread.tacv2%22%7D&tenantId=f733fdc5-1255-4d8d-b793-2bc7aca0f214) und füge zu jedem Pattern ein Emoji hinzu.

![add reaction](./x_gitressourcen/images/whiteboard_add_reaction.png)

Du kannst jedes Emoji benutzen. Allerdings haben diese Emojis eine vordefinierte Bedeutung:

| Emoji | Bedeutung |
| - | - |
![more](./x_gitressourcen/images/fire.png) | ich möchte mehr darüber erfahren
![got it](./x_gitressourcen/images/tick.png) | das habe ich verstanden - abgehakt
![no clue](./x_gitressourcen/images/question_mark.png) | keine Ahnung, wobei es darum geht



