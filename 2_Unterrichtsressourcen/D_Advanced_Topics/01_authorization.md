# Authorization

## Prinzipielle Vorgehensweise

Die am häufigsten verwendete Methode für die Authentifizierung mit Microservices ist ein Token.

Dabei erzeugt der Service für registrierte Benutzer eine Art Eintrittsticket, dass der Benutzer dann immer wieder benutzen kann, um sich auszuweisen. 

```plantuml

user -> service : register

user -> service : login

service -> user : token

user -> service : request (with token)

user -> service : request (with token)

```

für die Umsetzung es gibt mehrere Möglichkeiten:

1. alles selbst implementieren
1. bewährte Bibliotheken nutzen

_Faustregel:_
bei allem was mit Security zu tun hat, so wenig wie möglich selber machen.

---

Architektonisch gibt es ebenfalls mehrere Möglichkeiten:

1. Authentifizierung für jeden einzelnen Microservice entwickeln.
1. Authentifizierung auf einem sogenannten APIGateway entwickeln.
1. Einen externen Authentifizierungsservice (z.B. [auth0](https://auth0.com/) von Okta) nutzen. 


> __Aufgabe AUTH 1__ (Leseauftrag)

Lest euch das hier durch und merkt euch die Vor- und Nachteile der verschiedenen Ansätze.
[Leseauftrag](https://api7.ai/blog/understanding-microservices-authentication-services)

Wir entscheiden uns zunächst für die Variante einer Authentifizierung pro Microservice. Damit können wir in einer einfachen Umgebung die Grundkonzepte ausprobieren.

## User Management

Damit wir überhaupt das Thema Authentifizierung angehen können, brauchen wir eine solide Benutzerverwaltung. Wir können ja am Ende niemanden identifizieren, wenn wir ihn nicht kennen.

> __Aufgabe AUTH 2__ (Pair Programming)

Erweitere das bisherige Template um ein User-Management.
Über die API sollen neue Benutzer angelegt werden, Benutzer gelöscht werden und Benutzer nach ID abgefragt werden.

Ein Benutzer soll folgendermassen aussehen.

```plantuml
class User
{
    name : String (128)
    email : String (128) unique
    password : String (256)
}
```
> Hinweise:
> - Das Passwort-Feld soll absichtlich so lange sein - warum erfahren wir später.
> - Die E-Mail soll nur einmal im System vorhanden sein. Das bekommen wir bei der SQLAlchemy Definition mit dem Schlüsselwort [_unique_](https://docs.sqlalchemy.org/en/20/core/constraints.html#indexes) hin.
> - __Tests__ nicht vergessen.

## Password Hashing

Wir wollen nicht die Verantwortung für die sichere Speicherung des Passworts an uns haften haben. Sollte etwas mit der DB schiefgehen, können Passwörter, die auch irgendwo anders verwendet werden in falsche Hände gelangen.

> __Aufgabe AUTH 3__ (Leseauftrag)

Lest euch unter diesen Links das Wichtigste zum Thema durch.

- [Passwort Hashing (Datenschutzexperte.de)](https://www.datenschutzexperte.de/blog/datenschutz-im-unternehmen/passwort-hashing-salted-password-hashing/)
- [Wikipedia Hash-Funktion](https://de.wikipedia.org/wiki/Hashfunktion)

---

> __Aufgabe AUTH 4__ (Pair Programming)

Bevor ihr das Passwort des neuen Nutzers in der Datenbank ablegt, wendet ihr eine Hash-Funktion darauf an. 

Das geht am besten mit dem Python [_Werkzeug_-Paket](https://werkzeug.palletsprojects.com/en/3.0.x/).
Dieses Paket kommt standardmässig mit Flask mit und braucht nicht extra installiert zu werden. Es ist sozusagen das __Schweizer Taschenmesser__ bei der Entwicklung von Microservices mit Python.

Die Funktionen, die uns im Moment interessieren, sind, sind im [Security-Utility](https://werkzeug.palletsprojects.com/en/3.0.x/utils/#module-werkzeug.security) Bereich der Werkzeug Bibliothek.

Die Parameter _method_ und _salt_length_ können wir zunächst ignorieren und die Standardwerte benutzen.

Der beste Weg das Passwort als Hash abzuspeichern ist es, den Konstruktor der Klasse User zu überladen.

```python
   def __init__( self, email, name, password ):
        self.email = email
        self.name = name
        self.password = generate_password_hash(password)
```

## Login

Jetzt können wir uns an das Login machen.
Die Login Funktion soll überprüfen, ob 
1. der Nutzer in der Datenbank registriert ist, 
1. ob der Hash des gespeicherten Passworts mit dem Hash des mitgelieferten Passworts übereinstimmt.
1. ein Token zurückgeben.


---

> __Aufgabe AUTH 5__ (Leseauftrag)

Für den letzten Punkt müssen wir uns das Thema __Token__ nochmals genauer ansehen.

- [JWT erklärt bei Postman](https://blog.postman.com/what-is-jwt/)
- [JWT bei Wikipedia](https://de.wikipedia.org/wiki/JSON_Web_Token)

---

> __Aufgabe AUTH 6__ (Pair Programming)

Setzt die Login Funktion als Route in unserer Flask-App um und schützt eine Route mit einem Token.

Verwendet die Werkzeug Funktionen für den Hash-Abgleich (_check_password_hash_)

Für das JWT-Token empfehle ich: 
[pyJWT](https://pyjwt.readthedocs.io/en/stable/) als Package zu installieren. (nicht vergessen das Image neu zu bauen!)

> Hinweise:
> - da wir APIFlask nutzen, lohnt es sich diese Links auch durchzulesen:
>   - [APIFlask Authentification](https://apiflask.com/authentication/) 
>   - [APIFlask Reference Security](https://apiflask.com/api/security/)
> - hier ein [simples Beispiel von APIFlask](https://github.com/apiflask/apiflask/blob/main/examples/auth/token_auth/app.py), das die Umsetzung mit HTTPAuth aufzeigt.

hier sind die notwendigen Methoden für das login: 

```python
    def check_password(self, password):
        return check_password_hash(self.password, password)

    def generate_auth_token(self, expires_in = 600):
        exp_timestamp = int(datetime.now(timezone.utc).timestamp()) + expires_in
        return encode(
            { 'id': self.id, 'exp': exp_timestamp },
            current_app.config['SECRET_KEY'], algorithm='HS256')
```

> _Hinweis_ : - der SECRET_KEY ist in unserer app.config bereits vorgedacht. Man kann es über eine Env-Variable im Compose File setzen. __Achtung__: es macht Sinn, diesen Wert Standardmässig zu belegen, falls die ENV-Variable nicht gesetzt ist.

```python
SECRET_KEY = os.environ.get('SECRET_KEY') or 'very_secret_string'
```

damit können wir eine Login Route wie folgt generieren:

```python
@bp.post('/login')
@bp.input(LoginIn, location='json')
@bp.output(TokenOut, status_code=200)
def login_user(json_data):
    # Find user by email
    user = User.query.filter_by(email=json_data.get('email')).first()
    # If user doesn't exist or password is wrong
    if not user or not user.check_password(json_data.get('password')):
        return {'message': 'Invalid email or password'}, 401
    
    token = user.generate_auth_token(600)
    return { 'token': token, 'duration': 600 }
```

> _Hinweis:_ Das Token ist nur 10 Minuten gültig. Damit verhindert man, dass alte Token wiederverwendet werden können.

## Token Verification

Damit wir überprüfen können, dass es sich um ein gültiges token handelt, brauchen wir noch eine Methode in unserer Datei _user.py_ (als statische Methode der Klasse User). Statische Methoden haben den Vorteil, dass man sie auf der Klasse aufrufen kann und noch kein Objekt braucht.

hier ist die Methode zur Token-Verifikation:

```python
@staticmethod
    def verify_auth_token(token):
        try:
            data = decode(token, current_app.config['SECRET_KEY'], algorithms=['HS256'])
            return User.query.filter_by(id=data['id']).first()
        except ExpiredSignatureError:
            # Handle expired token, if necessary
            return None
        except InvalidTokenError:
            # Handle invalid token, if necessary
            return None
        except Exception as e:
            # Log or handle other exceptions
            print(f"An error occurred: {e}")
            return None
```

## Route Protection

Jetzt nutzen wir [Flask-HTTPAuth](https://flask-httpauth.readthedocs.io/en/latest/), um unsere kritischen Routen zu schützen.
APIFlask hat Flask-HTTPAuth bereits integriert.
Damit die Library funktioniert, müssen wir ihr noch mitteilen, wie ein Token validiert wird. 

Dafür verwenden wir einen Python-Decorator.
In [diesem Beispiel](https://circleci.com/blog/authentication-decorators-flask/) wird erklärt, wie ein decorator funktioniert, und wie man ihn für die Authentifizierung nutzen kann.

für HTTPAuth benötigen wir noch einen extra File _auth.py_ im Verzeichnis _app_. Dort können wir den Decorator definieren:

```python
from apiflask import HTTPTokenAuth
from app.models.user import User

token_auth = HTTPTokenAuth()

@token_auth.verify_token
def verify_token(token):
    user = User.verify_auth_token(token)
    if not user:
        return False
    return True
```

unsere Routen schützen wir dann mit dem decorator. Hier ein Beispiel für die Course-Route:

```python
from app.auth import token_auth

# ...

@bp.patch('/<int:course_id>')
@bp.auth_required(token_auth) # <-----
@bp.input(CourseIn(partial=True), location='json')
@bp.output(CourseOut)
def update_course(course_id, json_data):
    course = db.get_or_404(Course, course_id)
    for attr, value in json_data.items():
        setattr(course, attr, value)
    db.session.commit()
    return course
```

## Testing

Testet die Implementierung mit APIFlask/docs oder Postman und schreibt dann automatisierte Tests. 
__ACHTUNG__: die alten Tests werden nicht mehr funktionieren, wenn sie eine geschützte Route betreffen.

> - Tokens werden bei einer Anfrage im Header als _Bearer_ mitgegeben.

Hier ein Beispiel-Test

```python
def test_post_student(client):
    response0 = client.post("/users/login", json={'email': 'test@test.ch', 'password': 'test'})
    access_token = response0.json["token"]
    headers = { 'Authorization': 'Bearer {}'.format(access_token) }
    response = client.post("/students/", json={
            'name': 'Nina Hagen', 'level': 'AP'
        }, headers=headers)
    assert response.status_code == 201
```




