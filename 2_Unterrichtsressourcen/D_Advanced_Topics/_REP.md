# Wiederholung (10.04.2024)

## Flask Allgemein / REST
- Flask = Framework
- Webserver = Gunicorn
- Alternative Django
- REST für Skalierbarkeit
- Gute Balance im Design (siehe Amazon Prime Beispiel)

## Docker
- Alternative Podman
- Images Standardisiert
- Unterschied Kubernetes

## SQLAlchemy, ORM, CRUD
- noSQL / SQL
- Connector / MySQL

## Dev, Test, Prod - Compose
- HotReload, docker volume / bind
- Flask vs Gunicorn
- HTTP-Server vs Flask Framework
- PyTest

## GitLab CI
- GitlabFlow, Merge Request, Protected Branches

## Authentication / Authorization
- JWT. Password-Hashing

## Monitoring / Logging
- Flask-Monitor 
