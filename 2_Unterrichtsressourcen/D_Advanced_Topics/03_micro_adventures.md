# Micro Adventures

Microservices in Action

## Dein erstes Micro Adventure

> __Aufgabe MADV 1__ (Einzelübung)

Aktiviert das Logging in der Microservice Applikation (im file app/users/routes.py) mit


- gehe auf http://msvc.vonlangerhand.net
- Erstelle einen Account (login -> register)
- öffne das Rooms Menü
- starte den Test für Demo-Room
- Löse den Demo-Room
- notiere alles, was Dir auffällt (Fehler, unerwünschtes Verhalten etc.)


> __Aufgabe MADV 2__ (Gruppenübung)

Diskutiert die Findings zusammen mit dem Kursleiter und erstellt gemeinsam Tickets in GitLab

> __Aufgabe MADV 2__ (Pair-Programming)

Macht einen Fork von

https://gitlab.com/tbz-itcne23-msvc/ma-demo-room

und entwickelt euren eigenen Room




