# Microservices - Asynchrone Kommunikation

bisher haben wir RESTful APIs benutzt, um zwischen Microservices zu kommunizieren.
Wie bei einem Funkgerät, hat dieser Kommunikationsstil den Nachteil, dass immer nur ein Teilnehmer zur gleichen Zeit sprechen kann. Bei vielen Teilnehmern ist das manchmal etwas "nervig".

Als Alternative gibt es die Asynchrone Kommunikation. Da könne alle gleichzeitig sprechen. Das System muss sich dann überlegen, in welcher Reihenfolge die einzelnen Nachrichten abgehandelt werden.

> __Aufgabe ASYN_1__ (Leseauftrag)

Nimm Dir 15 Minuten Zeit, und lies Dir die folgenden Quellen durch:

- [Wikipedia - asynchrone Kommunikation](https://de.wikipedia.org/wiki/Asynchrone_Kommunikation)
- [Redis - in memory (noSQL) DB](https://redis.io/docs/latest/get-started/)
- [Redis - pub/sub](https://redis.io/docs/latest/develop/interact/pubsub/)


---

> __Aufgabe ASYN_2__ (Pair Programming)

Klone [dieses Repository](https://gitlab.com/tbz-itcne23-msvc/blueprint-flask-redis) und starte einen Chat mit Deinen Kollegen.

---

> __Aufgabe ASYN_3__ (Pair Programming)

Erweitere das Programm um folgende Aspekte:

1. Speichere alle gesendeten Messages in der Datenbank.
2. Fortgeschritten: ermögliche es, verschiedenen Chat-Channels beizutreten und eigene Chat-Channels zu erstellen.

