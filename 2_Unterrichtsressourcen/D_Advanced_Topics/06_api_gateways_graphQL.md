# API Gateways und Alternativen zu REST (GraphQL)

## API Gateways

### Motivation

Je weiter wir unsere Applikation in kleinere Einheiten aufteilen, desto spezialisierter sind die einzelnen APIs. Zusätzlich wird die Applikation verschiedene Clients bedienen müssen. Zum Beispiel eine Native App auf iOS oder eine SPA Applikation optimiert für Desktops. Jeder dieser Clients hat unterschiedliche Bedürfnisse.

Anstatt es dem Client zu überlassen, die verschiedenen Endpunkte zu finden und sich alle Informationen zusammenzustellen, können wir das [API-Gateway Pattern](https://microservices.io/patterns/apigateway.html) verwenden.

Dabei kümmert sich das Gateway darum, dass jeder Client die für ihn relevanten Informationen von den dahinter geschalteten MicroServices erhält.

### Übersicht

```plantuml
skinparam monochrome true
skinparam packageStyle rectangle
hide circle
hide empty members

class "iOS App" as app
class "Vue SPA" as web
class "Purchase Portal" as portal

package backend {

class "API Gateway" as gateway

class "Logistics (API)" as api1
class "Pricing (API)" as api2
class "Payment (API)" as api3

class "DB 1" as db1
class "DB 2" as db2

gateway -- api1
gateway -- api2
gateway -- api3

api1 -- db1
api2 -- db1
api3 -- db2

}

app -- gateway
web -- gateway
portal -- gateway

```

---

> __Aufgabe GTWA 1__ (Leseauftrag)

Lies bei RedHat mehr über das Thema [API Gateway](https://www.redhat.com/en/topics/api/what-does-an-api-gateway-do) nach.


## GraphQL

### Motivation

Je komplexer die Orchestrierung der verschiedenen Microservices wird, desto schwieriger wird es, die APIs auf dem neuesten Stand zu halten. 

Irgendwann ist es nicht mehr möglich, einfach alle verfügbaren Informationen eines Microservices über das Netzwerk zu schicken, obwohl das meiste davon von einem bestimmten Client gar nicht benötigt wird.

Deshalb hat sich eine Alternative zu den starren REST Schnittstellen entwickelt, die sich in der Zwischenzeit weit verbreitet hat - [__GraphQL__](https://graphql.org/).

### Lesestoff

> __Aufgabe GTWA 2__ (Leseauftrag)

Schau Dir die [Einführung zum Thema GraphQL](https://www.redhat.com/en/topics/api/what-is-graphql) bei RedHat an.

### GraphQL in der Praxis

> __Aufgabe GTWA 3__ (Pair Programming)
Damit wir GraphQL selbst mal erleben können, spielen wir das zusammen durch.

Als erstes cloned bitte dieses repo:
https://gitlab.com/tbz-itcne23-msvc/blueprint-flask-graphql


Wechselt auf den Branch _graphql_integration_

Startet den Server mit

```bash
docker compose up --build
```

get auf

```bash
http://localhost:5000/docs
```

fügt über die API neue Studenten hinzu


Wechselt im Browser auf 

```bash
localhost:5000/graphql
```

Versucht dann folgende Query:


```
query fetchAllStudents {
  students {
    success
    errors
    students {
      name
      level
      id
    }
  }
}
```


---

ab hier noch nicht weitermachen 
TODO: Tutorial ist noch nicht fertig.
---

erstellt eine Datei _schema.graphql_ im Verzeichnis app des repos mit folgendem Inhalt:

```python
schema {
    query: Query
    mutation: Mutation
}

type Student {
    id: ID!
    name: String!
    level: String!
}

type StudentResult {
    success: Boolean!
    errors: [String]
    student: Student
}

type StudentsResult {
    success: Boolean!
    errors: [String]
    students: [Student]
}

type Query {
    students: StudentsResult!
    student(studentId: ID!): StudentResult!
}

type DeleteStudentResult {
    success: Boolean!
    errors: [String]
}

type Mutation {
    createStudent(name: String!, level: String!): StudentResult!
    deleteStudent(studentId: ID!): DeleteStudentResult!
    updateLevel(studentId: String, newLevel: String!): StudentResult!
}

```

fügt als nächstes der Student Klasse im File _app/models/student.py_ eine function _to_dict()_ hinzu:

```python
class Student(db.Model):
    __tablename__ = 'students'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32))
    level = db.Column(db.String(8))
    courses = db.relationship('Course', secondary='registrations', back_populates='students')

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "level": self.level
        }
```

jetzt brauchen wir einen query resolver. Legt dafür eine Datei _queries.py_ Verzeichnis app mit folgendem Inhalt an:

```python
from app.models.student import Student


def resolve_students(obj, info):
    try:
        students = [student.to_dict() for student in Student.query.all()]
        payload = {
            "success": True,
            "students": students
        }
    except Exception as error:
        payload = {
            "success": False,
            "errors": [str(error)]
        }
    return payload
```

In dieser Datei definieren wir, wie wir die Anfragen aus dem Schema aus der Datenbank erfüllen. Wir verbinden also graphQL mit unserer MySQL DB.

Als graphQL Library nutzen wir [Ariadne](https://ariadnegraphql.org/). Dazu fügen wir in unsere _requirements.txt_ folgende Zeile ein:

```python
ariadne==0.23.0
```

