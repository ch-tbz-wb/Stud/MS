# Microservice Monitoring

Damit wir wissen, wie unser Microservice in der Praxis so benutzt wird, wäre es praktisch während dem Betrieb ein paar Daten zu sammeln.

## Logging

Jeder gute Microservice sollte ein Logging implementieren.
Zum Glück bietet Python bereits eine Standard-Lösung dafür ([Python Logging](https://docs.python.org/3/howto/logging.html)).

 __Aufgabe MONI 1__ (Pair-Programming)

Aktiviert das Logging in der Microservice Applikation (im file app/users/routes.py) mit

```python
import logging
```

Erstellt einen Log Eintrag wenn sich ein User einloggt bzw. dabei scheitert.

Im Terminal könnt ihr die aktuellen logs aus dem Docker Container holen:

```bash
docker logs <container-name>
```

Testet das Logging indem ihr euch einloggt und dann den log auslest.


## Route Monitoring

Später werden wir wissen wollen, welche unserer Routes besonders häufig benutzt werden und wo sich eventuell Flaschenhälse bilden.

Dazu bietet sich eine sehr komfortable Erweiterung für Flask an: [Das Flask Monitoring Dashboard](https://flask-monitoringdashboard.readthedocs.io/en/v1.10.3/).

> __Aufgabe MONI 2__ (Pair-Programming)

Installiert das Flask Monitoring Dashboard in eurem aktuellen Microservice.

---

Fügt die Library in unsere requirements.txt ein:

```bash
Flask-MonitoringDashboard==3.3.0
```

Die Library wird in unsere Flask App folgendermassen eingebunden:

in der 

```python

# File app/__init__.py

from apiflask import APIFlask

...

import flask_monitoringdashboard as dashboard

...

dashboard.bind(app)

return app

```

Danach können wir unseren Microservice lokal neu bauen und starten

```bash
docker compose up --build
```

Ihr könnt den Monitor im Browser aufrufen mit

```bash
localhost:5000/dashboard
```

das Login in Standardmässig auf __admin, admin__ konfiguriert.

---

Die Monitoring Library erzeugt einen db-File im Verzeichnis unseres Source-Codes.

> __Aufgabe MONI 3__ (Pair-Programming)

Sorgt dafür, dass die der Datenbankfile in einem Docker-Volume gespeichert wird.

Dazu müsst ihr die Konfiguration des Monitors anpassen.

mehr dazu in der [offiziellen Doku](https://flask-monitoringdashboard.readthedocs.io/en/latest/configuration.html)

