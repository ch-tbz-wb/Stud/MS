# D - Advanced Topics

[[_TOC_]]

wir sind soweit, dass wir weiterführende Konzepte erarbeiten können.
Danei werden wir uns überwiegend an den bekannten [Microservice Patterns](http://microservices.io) orientieren.

## Kompetenzen

- B10.1 Die Architektur der Software bestimmen und die Entwicklung unter Berücksichtigung von Betrieb und Wartung planen und dokumentieren (Niveau: 3)

## Authentifizierung und Authorizierung

Damit wir Kontrolle darüber haben, wer unsere Microservices benutzt brauchen wir Methoden, um Nutzer zu identifizieren und ihren Zugriff einzuschränken.

### Hands on

[AuthN und AuthZ](./01_authorization.md)


## Links

*Allgemeine Links welche sich nicht in die Unterthemen einordnen lassen*