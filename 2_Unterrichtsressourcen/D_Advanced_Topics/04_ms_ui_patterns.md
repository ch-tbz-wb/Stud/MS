# Benutzerschnittstellen für Microservices

normalerweise hat ein Microservice keine Benutzerschnittstelle. Ein- und Ausgabe erfolgt über die Restful API und nicht über eine Webseite.

Trotzdem kann es manchmal nützlich sein, ein grafisches Frontend zur Verfügung zu stellen.

Dafür gibt es prinzipiell 2 Möglichkeiten:

1. __Server Side Rendering (SSR)__
    Dabei wird der HTML Code bei einer Anfrage an einen bestimmten Endpunkt generiert und and den aufrufenden Browser zurückgegeben. Das setzt natürlich voraus, dass die Route von einem Browser aufgerufen wurde.

    In Flask wird das mit sogenannten Templates realisiert.
    Ein gutes Tutorial findet sich hier: https://www.digitalocean.com/community/tutorials/how-to-use-templates-in-a-flask-application

2. __Single Page Application (SPA)__
    bei modernen Frontends wird der HTML Code auf dem Client gehalten und per Javascript manipuliert. Dieser Ansatz heisst deshalb Single Page, da meistens nur eine einzige HTML-Seite vom Webserver geladen wird und diese ihren Inhalt verändert.

## Erweiterung des Blueprints um eine SSR Page

Als Beispiel erweitern wir unsere bestehende Flask Applikation um eine Route, die eine HTML-Seite mit Inhalten vom Server anzeigt.

Dazu legen wir unterhalb des Verzeichnisses _app_ ein neues Verzeichnis _templates_ an. In diesem Verzeichnis sucht Flask nach HTML-templates.

Innerhalb dieses Verzeichnisses erstellen wir eine Datei _index.html_ mit folgendem Inhalt:

```html

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>FlaskApp</title>
</head>
<body>
    <h1>Hello World!</h1>
    <h2>Welcome to FlaskApp!</h2>
    <h3>{{ utc_dt }}</h3>
</body>
</html>

```

Dann legen wir eine zusätzliche Route in unserer Flask app an:

```python
   from flask import render_template
   import datetime

   ...

   @app.route('/index')
    def show_index():

        context = {
            'utc_dt': datetime.datetime.now(datetime.UTC)
        }

        return render_template('index.html', **context)
```

wenn wir nun _localhost:5000/index_ im Browser aufrufen, dann sehen wir eine HTML-Seite, die das aktuelle Datum und die Uhrzeit anzeigt. Das Datum wurde der Methode render_template als context übergeben.

Auf diese Art und Weise können wir auch Inhalte aus der Datenbank ium Browser nutzerfreundlich darstellen.


## SPA mit Vue

Mit Vite und Vue können wir auch Single Page apps erstellen.
Auf dieser Seite ist beschreiben, wie man schnell ein SPA-Gerüst erzeugt: https://vitejs.dev/guide/

Zur Interaktion mit dem Backend/Flask-Server kann dann axios (https://axios-http.com/docs/intro) verwenden.

Moderne Browser haben bereits eine HTTP-Schnittstelle eingebaut (https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch). Das ist ebenfalls eine Variante, um API-Funktionen vom Backend aufzurufen. 
