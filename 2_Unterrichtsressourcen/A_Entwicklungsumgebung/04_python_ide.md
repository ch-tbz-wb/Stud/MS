# IDE für die Entwicklung installieren

VSCode ist eine kostenlose und leichtgewichtige IDE, die von einem Microsoft-Team in der Schweiz entwickelt wird. Da wir in unseren Übungen auch andere Technologien als nur Python verwenden werden, empfehle ich die Verwendung von VSCode. Selsbtverständlich sind alle Übungen auch mit PyCharm zu bewältigen.

Lade den VSCode-Installer [hier](https://code.visualstudio.com/Download) herunter und installiere ihn. 
 
Wir benötigen folgende VSCode Extensions:

- Python - offizielle Extension von Microsoft
- Docker - offizielle Extension von Microsoft

Gehe in die Ubuntu bash und lege mit
```bash
mkdir repos
cd repos
```
ein Verzeichnis an. 

Wechsle in das neue Verzeichnis.

Nun kannst Du dieses Verzeichnis mit VSCode auf dem Windows-Host öffnen.
```bash
code . 
```
