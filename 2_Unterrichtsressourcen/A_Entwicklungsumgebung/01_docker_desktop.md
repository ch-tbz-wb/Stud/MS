# Docker Desktop Installieren

Docker ist der Standard für die Entwicklung von Microservices. Aufgrund von diversen politischen Entscheidungen der Firma hinter Docker, hat sich die Alternative Podman entwickelt. Wir werden in diesem Modul aber bei Docker bleiben, weil es ein wenig komfortabler ist. 

Falls Du mutig bist, kannst Du natürlich auch Podman auf eigene Faust erkunden. Theoretisch sollten alle Beispiele auch damit funktionieren.

Lade [hier](https://www.docker.com/products/docker-desktop/) Docker Desktop herunter und installiere es.

