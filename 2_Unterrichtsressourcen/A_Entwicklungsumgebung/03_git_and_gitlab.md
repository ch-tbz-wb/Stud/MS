# Git konfigurieren

Git ist heutzutage das am meisten benutzte Versionsverwaltungsprogramm. Es wurde ursprünglich von Linus Torval entwickelt, um die Zusammenarbeit von vielen freiwilligen Entwicklern bei der Entstehung von Linux zu vereinfachen.

Damit wir git mit Gitlab nutzen können, müssen wir ein paar Dinge konfigurieren.

```bash
ssh-keygen -t ed25519 -C [ihre@email.ch]
git config --global user.email [ihre@email.ch]
git config --global user.name [ihr gitlab_username]
git config --global --list
```

Kopiere den Inhalt der Datei id_ed25519.pub aus dem Verzeichnis .ssh innerhalb Deines Home-Verzeichnis in der Linux bash in einen neuen SSH-Schlüssel in Deinen GitLab-Settings.

Jetzt kannst Du private Repositories von GitLab klonen und selbst Source Code auf GitLab schieben.