# Eine Linux Umgebung für die Entwicklung 

## Einleitung

Die Linux bash hat sich als ein Standard für die Software Entwicklung und die Verwaltung von Servern etabliert. Das liegt unter anderem daran, dass die meisten Cloud-Server mit Linux betrieben werden.

Es ist deshalb wichtig, dass wir eine gut funktionierende Linux-Umgebung auf unserem Entwicklungssystem zur Verfügung haben.

Zum Glück hat sich Microsoft in den letzten Jahren gegenüber Linux geöffnet und mit dem Windows Subsystem for Linux (WSL) eine sehr gute Integration zwischen Windows und Linux geschaffen.

## Installation

Öffne die Eingabeaufforderung unter Windows (cmd)

Starte die Installation von WSL mit:
```bash
wsl --install
 ```
Wenn man keine spezielle Linux-Distribution als Parameter übergibt, wird standardmässig Ubuntu installiert. Das ist für unsere Zwecke ausreichend.

Folge den Anweisungen des Installationsskripts und lege einen Benutzernamen und ein Passwort für fest.
Achtung: bitte Nutzername und Passwort gut merken. Sonst hat man später keinen Zugriff auf das Linux Subsystem.

Verlasse die shell mit dem Befehl *exit* und kehre zur Windows Eingabeaufforderung zurück.

Überprüfe die WSL Version mit

```bash
wsl -l -v 
```
Wir können den neuen Shortcut "Ubuntu" aus dem Startmenu in Zukunft verwenden, um unsere Linux shell zu öffnen.

## Docker auf WSL aktivieren

1. Öffne Docker Desktop
2. gehe zu den Einstellungen (Zahnrad oben rechts)
3. Wähle *respurcen*
4. wähle *WSL*
5. aktiviere *ubuntu*
 
## Sonstiges

### Systemzeit
achte darauf, dass die Systemzeit zwischen Windows und WSL synchron ist.

Überprüfen mit:
```bash
timedatectl
```
Synchronisieren mit:
```bash
sudo hwclock -s
```

## Troubleshooting

Probleme wegen Virtualisierung: 
Schritt 1: Ist das Windows auf Pro? Wenn nein und Privat, dann hier Edu-Lizenz  holen und bei windows aktivieren (ohne neu installation):

https://azureforeducation.microsoft.com/devtools

Dann testen ob Hyper-V läuft.
 
Schritt 2: Die Lösungen von hier einzeln durchgehen 

https://stackoverflow.com/questions/39684974/docker-for-windows-error-hardware-assisted-virtualization-and-data-execution-p



