# A - Entwicklungsumgebung aufsetzen

[[_TOC_]]

Transform your notebook into a professional development studio

Bevor wir loslegen, wird es Zeit, sich die richtigen Werkzeuge zurechtzulegen.

![Zeit nehmen, um die Säge zu schärfen](https://sinnstiften.biz/wp-content/uploads/2021/11/keine-zeit-saege-schaerfen.jpg)

Oft sind wir so gefangen in unseren täglichen Aktivitäten, dass wir gar nicht merken wo wir eigentlich viel effizienter sein könnten. Es macht deshalb Sinn, ein wenig Zeit in die Auswahl und Konfiguration unserer Werkzeuge zu investieren.

Die folgenden Werkzeuge sind lediglich Vorschläge. Natürlich gibt es für jedes Werkzeug unzählige gleichwertige Alternativen. 


## Kompetenzen
- A1.7 Zusammenarbeit im Team gestalten, reflektieren und Regeln vereinbaren (Niveau: 3)
- A2.6 Die branchenspezifischen Fachtermini des Engineerings verwenden und diese adressatengerecht kommunizieren
- B4.6 Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen (Niveau: 3)

## 1 - Docker Desktop - Container lokal verwalten und betreiben

Container sind ein wesentlicher Bestandteil von Microservices. Nur durch die Einführung der leichtgewichtigen und skalierbaren Container-Konzepte ist es möglich Services in der Cloud unabhängig von der zugrunde liegenden Infrastruktur kosteneffizient zu warten und zu betreiben.

### Lernziele / Taxonomie 

- kann eine Entwicklungsumgebung für Container aufsetzen, konfigurieren und verwenden.

### Alternativen

https://podman-desktop.io - 
Podman ist eine gute Alternative zu Docker. Da Docker Desktop seit einiger Zeit Lizenzgebühren verlangt, hat sich in der Container-Community eine gleichwertige Alternative entwickelt. 

### Hands-on

[Docker Desktop installieren](./01_docker_desktop.md)


## 2 - Linux Shell für die Entwicklung

Linux eignet sich aus vielerlei Sicht besser für die Entwicklung von Microservices als die MS Powershell.

### Lernziele / Taxonomie 

- kann eine Linux Umgebung unter Windows aktivieren/installieren oder eine Linux Distribution auf einem Rechner installieren.

### Alternativen

https://www.cygwin.com - um eine vollwertige Linux Umgebung unter Windows zu bekommen wurde in der Vergangenheit of auf Cygwin zurückgegriffen. Allerdings ist die Installation komplizierter und die Integration mit dem Host Betriebssystem ist nicht so gut wie bei WSL.

### Hands-on

[WSL unster Windows in Betrieb nehmen](./02_linux_on_windows.md)


## 3 - Versionsverwaltung in der Cloud mit GitLab

Damit später die Vorteile von Public Container Registries und CI/CD genutzt werden können ist ein GitLab Account notwendig. Das lokale Linux (Subsystem) muss mit einem ssh-public key mit dem GitLab-Account verknüpft werden.


### Lernziele / Taxonomie 

- kann einen GitLab-Account mit einem lokalen git Repository verbinden.

### Alternativen

https://github.com - GitHub ist weit mehr verbreitet als GitLab. GitHub wurde von Microsoft aufgekauft. Deshalb haben sich viele kleinere Projekte für GitLab entschieden. GitLab ist das gesetzte Versionsmanagementsystem an der TBZ. GitLab erlaubt beliebig viele private Repositories.

### Hands-on

[GitLab mit lokalem git verknüpfen](./03_git_and_gitlab.md)


## 4 - IDE für die Entwicklung von Microservices

VSCode ist eine starke IDE, die mit vielen Erweiterungen für WSL, Docker, Python etc. erweitert werden kann. Wenn mehr als nur Python benutzt wird, ist sie eine gute Wahl.


### Lernziele / Taxonomie 

- kann VSCode installieren und vom Windows Subsystem for Linux aus starten.

### Alternativen

https://www.jetbrains.com/de-de/pycharm/ - PyCharm ist eine sehr gute IDE für Python. Allerdings ist die wirklich gute Version kostenpflichtig. Weiterhin bietet VSCode eine bessere Integration von anderen Technologien.

### Hands-on

[VSCode installation und Verknüpfung mit WSL](./04_python_ide.md)




## Links

*Allgemeine Links welche sich nicht in die Unterthemen einordnen lassen*

