# MSVC - Microservices mit Python

 - Bereich: System- und Netzwerkarchitektur bestimmen
 - Semester: 3

## Lektionen

* Präsenz: 80
* Virtuell: 0
* Selbststudium: 50


## Fahrplan

| **Lektionen** | **Selbststudium** | **Kompetenzbereich**                                | **Kompetenzenband**    | **Tools**                                   |
|-------------|-------------------|-------------------------------------------------------|------------------------|--------------------------------------------|
| 6           | 4                 | Grundlagen der Microservices verstehen         | a)  | Visualisierungen von Microservice-Architekturen, Diagrammtools (z.B. Mermaid, PlantUML) |
| 10          | 6                 | Docker-Umgebungen aufsetzen                    | b)  | Docker, Docker Compose, Linux-Terminal     |
| 8           | 5                 | Versionierung und Zusammenarbeit mit Git/GitLab| c)  | Git, GitLab, GitHub                       |
| 10          | 6                 | Microservices lokal bereitstellen und testen   | d)  | Flask, Postman, Docker, Python             |
| 8           | 5                 | Persistenzschichten mit MySQL einrichten       | e)  | MySQL, Docker, Python ORM (z.B. SQLAlchemy)|
| 8           | 5                 | Testen und Automatisieren von Microservices    | f)  | pytest, GitLab CI/CD              |
| 10          | 6                 | Deployment in Produktionsumgebungen            | g)  | Docker, Kubernetes, Helm                  |
| 6           | 4                 | Monitoring und Überwachung von Microservices   | h)  | Prometheus, Grafana                       |
| 10          | 6                 | Skalierung und Struktur von Microservices verstehen | i)  | Kubernetes, Docker Swarm, Load Balancer   |
| 4           | 3                 | Asynchrone Kommunikation und API Gateways einsetzen | j)  | Redis, RabbitMQ, Kafka, GraphQL API              |
| **Total: 80**| **Total: 50**     |                                                       |   |                                           |


## Voraussetzungen:

Grundkenntnisse in Python, Docker, Git; Module: ITCNE, FAAS

## Lernziele

1. **Grundlagen der Microservices verstehen**: Die Studierenden sollen die Konzepte von Microservices, deren Vorteile und Herausforderungen im Vergleich zu monolithischen Architekturen verstehen.

2. **Docker-Umgebungen aufsetzen**: Die Studierenden sollen in der Lage sein, eine lokale Entwicklungsumgebung für Microservices mit Docker Desktop und Linux auf Windows zu konfigurieren.

3. **Versionierung und Zusammenarbeit mit Git und GitLab**: Die Studierenden sollen lernen, wie man Git und GitLab für Versionskontrolle und kollaborative Entwicklung einsetzt.

4. **Microservices lokal bereitstellen und testen**: Die Studierenden sollen Microservices lokal mit Flask und einer minimalen Docker-Umgebung erstellen, testen und skalieren können.

5. **Persistenzschichten mit MySQL einrichten**: Die Studierenden sollen die Fähigkeit erwerben, Datenbanken wie MySQL in Microservice-Architekturen zu integrieren und die Persistenz von Daten sicherzustellen.

6. **Testen und Automatisieren von Microservices**: Die Studierenden sollen lernen, wie man Teststrategien für Microservices entwickelt und automatisierte Tests durchführt, um die Qualität des Codes zu gewährleisten.

7. **Deployment in Produktionsumgebungen**: Die Studierenden sollen verschiedene Deployment-Strategien kennenlernen, um Microservices sicher und effizient in Produktionsumgebungen bereitzustellen.

8. **Monitoring und Überwachung von Microservices**: Die Studierenden sollen Tools und Techniken kennenlernen, um Microservices in einer Produktionsumgebung zu überwachen und deren Gesundheit zu gewährleisten.

9. **Skalierung und Struktur von Microservices verstehen**: Die Studierenden sollen in der Lage sein, skalierbare Strukturen für Microservices zu entwerfen und die Herausforderungen von Lastverteilung und Skalierbarkeit zu meistern.

10. **Asynchrone Kommunikation und API Gateways einsetzen**: Die Studierenden sollen lernen, wie man asynchrone Kommunikationsmuster in Microservice-Architekturen implementiert und API-Gateways zur Verwaltung von Anfragen nutzt, z. B. mit GraphQL.


## Dispensation

- Gemäss Reglement HF Lehrgang

## Technik

Python, IDE mit Python-Unterstützung (VSCode, Pycharm), Docker, Podman, AWS Academy Kursumgebung, GitLab Account

## Methoden

Praktische Laborübungen, Anwenden von Blueprints, Coaching durch die Lehrperson, Test Driven Development, SW-Design mit UML

## Schlüsselbegriffe: 

- Microservices: Architektur, Skalierbarkeit, REST, Proxy, Persistenz, Monitoring, Logging, Container
- Python: Flask, SQLAlchemy, Redis, Marshmallow

## Lehr- und Lernformen: 

Lehrervorträge, Lehrgespräche, Workshop, Gastreferenten, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

## Lehrmittel:  

- Eberhard Wolff - Microservices, Grundlagen flexibler Softwarearchitekturen
- http://www.microservices.io




