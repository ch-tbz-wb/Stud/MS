# Handlungskompetenzen

## Kompetenzmatrix

|  **Kompetenzband**                                         | **RP**                                  | **HZ** | **Novizenkompetenz**                                 | **Fortgeschrittene Kompetenz**                      | **Kompetenz professionellen Handelns**              | **Kompetenzexpertise**                              |
|-------------------------------------------------------|----------------------------------------------------|-------|----------------------------------------------------|----------------------------------------------------|----------------------------------------------------|----------------------------------------------------|
| a) Grundlagen der Microservices verstehen           | B12.1                                              | 1     | Grundverständnis von Microservices                  | Vergleich von Vor- und Nachteilen                   | Erläuterung von Vor- und Nachteilen im Team         | Strategische Entscheidung für Architekturwechsel    |
| b) Docker-Umgebungen aufsetzen                      | B12.3                                              | 2     | Einfache Docker-Container erstellen                 | Docker-Umgebung aufsetzen und konfigurieren         | Fehlerbehebung und Optimierung der Umgebung         | Architektur komplexer Docker-Umgebungen            |
| c) Versionierung und Zusammenarbeit mit Git/GitLab   | B12.4                                              | 3     | Grundlagen von Git-Befehlen und Versionskontrolle   | Branching-Strategien und Konfliktlösung             | CI/CD-Pipeline implementieren und verwalten         | Teamwork mit komplexen Multi-Repo-Projekten         |
| d) Microservices lokal bereitstellen und testen      | B11.2                                              | 4     | Flask-basierte Microservices lokal starten          | Dockerisierte Microservices testen und integrieren  | Skalierbare Microservice-Strukturen erstellen       | Design und Architektur komplexer Service-Netzwerke  |
| e) Persistenzschichten mit MySQL einrichten          | B11.3                                              | 5     | Datenbank in Microservice integrieren               | Datenbankcontainer aufsetzen und konfigurieren      | Optimierung der Datenpersistenz und Leistung        | Architektur hochverfügbarer, verteilter Datenbanken |
| f) Testen und Automatisieren von Microservices       | B13.2                                              | 6     | Grundlagen von Unit-Tests                          | Automatisierte Tests und einfache CI/CD-Pipelines   | Implementierung von umfassenden Teststrategien      | Komplexe Testumgebungen mit hoher Skalierbarkeit    |
| g) Deployment in Produktionsumgebungen              | B12.4                                              | 7     | Einfaches Deployment in Produktionsumgebungen       | Deployment-Strategien entwickeln und umsetzen       | Deployment-Pipeline optimieren und skalieren        | Automatisierung und Selbstheilung im Deployment     |
| h) Monitoring und Überwachung von Microservices      | B12.1                                              | 8     | Grundlagen von Monitoring-Tools kennenlernen        | Konfiguration von Überwachungstools                 | Analyse und Fehlerbehebung in Produktionsumgebungen | Implementierung robuster Monitoring-Infrastrukturen |
| i) Skalierung und Struktur von Microservices verstehen | B12.3                                              | 9     | Skalierung von einfachen Services                   | Load-Balancer konfigurieren und Lastverteilen       | Design skalierbarer Architekturen                   | Konzeption und Implementierung elastischer Architekturen |
| j) Asynchrone Kommunikation und API Gateways einsetzen| B11.1                                              | 10    | Grundlagen von API-Gateways und asynchroner Kommunikation | Implementierung einfacher API-Gateways und Broker  | Optimierung von Kommunikationsstrukturen            | Konzeption von globalen, verteilten Kommunikationssystemen |


RP = Rahmenlehrplan
HZ = Handlungsziele

---

## Rahmenlehrplan

### Modulspezifische Handlungskompetenzen 

 * B12 System- und Netzwerkarchitektur bestimmen
   * B12.1 Die bestehende Systemarchitektur beurteilen und weiterentwickeln
   * B12.3 Bestehende ICT Konfigurationen analysieren, Umsetzungsvarianten für die Erweiterung definieren und Soll-Konfigurationen entwickeln
   * B12.4 Die Anforderungen an ein Konfigurationsmanagementsystem einer ICT-Organisation erheben und mögliche Lösungsvarianten vorschlagen

 * B11 Applikationen entwickeln, Programme erstellen und testen
   * B11.1 Vorgaben für die Konzipierung eines Softwaresystems mit einer formalen Methode analysieren
   * B11.2 Systemspezifikation interpretieren und die technische Umsetzung entwerfen
   * B11.3 Spezifikation in einer geeigneten Programmiersprache umsetzen

 * B13 Konzepte und Services entwickeln
   * B13.2 Spezifische Testkonzepte erstellen und die Tests der relevanten Prüfobjekte planen

### Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen. Die Handlungskompetenzen werden in vier Anforderungsniveaus eingestuft.

#### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

#### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

#### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.

#### Kompetenzniveau 4: Kompetenzexpertise

Entwickeln innovative Lösungen in einem komplexen Tätigkeitsfeld; antizipieren Veränderungen in der Zukunft und handeln proaktiv; übernehmen strategische Verantwortung und treiben Veränderungen und Entwicklungen voran.
