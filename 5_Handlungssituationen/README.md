# Handlungsziele und Handlungssituationen 

Zu jedem einzelnen Handlungsziel aus der Modulidentifikation wird eine realistische, beispielhafte und konkrete Handlungssituation beschrieben, die zeigt, wie die Kompetenzen in der Praxis angewendet werden.


### 1. **Grundlagen der Microservices verstehen**
**Handlungssituation**: Lena arbeitet in einem Team, das ein monolithisches System auf Microservices umstellen soll. Sie erhält die Aufgabe, dem Team zu erklären, warum der Wechsel sinnvoll ist, welche Vor- und Nachteile dieser Architekturwechsel mit sich bringt und welche Herausforderungen zu erwarten sind.

### 2. **Docker-Umgebungen aufsetzen**
**Handlungssituation**: Ein neuer Kollege im Team hat Schwierigkeiten, eine einheitliche Entwicklungsumgebung einzurichten. Jonas hilft ihm, Docker Desktop zu installieren und eine Microservice-basierte Umgebung unter Verwendung von Docker-Containern auf seinem Rechner zu konfigurieren, damit die gesamte Entwicklung standardisiert und portabel ist.

### 3. **Versionierung und Zusammenarbeit mit Git und GitLab**
**Handlungssituation**: Ein Teamprojekt steht an, bei dem mehrere Entwickler parallel an unterschiedlichen Microservices arbeiten. Anna wird gebeten, ein GitLab-Projekt einzurichten, Branching-Strategien zu erklären und zu demonstrieren, wie Merge-Requests verwendet werden, um Konflikte zu vermeiden und die Zusammenarbeit zu koordinieren.

### 4. **Microservices lokal bereitstellen und testen**
**Handlungssituation**: Ein Kunde wünscht ein neues Feature in der bestehenden Anwendung, welches über einen neuen Microservice realisiert werden soll. Max erstellt lokal eine minimalistische Flask-basierte API mit Docker, testet sie mit Postman und stellt sicher, dass sie mit den bestehenden Services interagiert.

### 5. **Persistenzschichten mit MySQL einrichten**
**Handlungssituation**: In einem bestehenden Microservice-System soll eine Funktion zur Speicherung von Benutzerdaten integriert werden. Sophie erhält die Aufgabe, eine MySQL-Datenbank in das System einzubinden, einen Datenbankcontainer mit Docker zu starten und sicherzustellen, dass der Microservice korrekt mit der Datenbank kommuniziert und Daten persistent speichert.

### 6. **Testen und Automatisieren von Microservices**
**Handlungssituation**: Ein Bug wurde in der Produktion entdeckt, der auf unzureichende Tests in der Entwicklungsumgebung zurückzuführen ist. Tim entwickelt Unit-Tests für die einzelnen Microservices und richtet eine CI/CD-Pipeline in GitLab ein, die automatische Tests nach jedem Commit durchführt, um die Qualität des Codes sicherzustellen.

### 7. **Deployment in Produktionsumgebungen**
**Handlungssituation**: Julia wird gebeten, einen neuen Microservice, der lokal bereits erfolgreich getestet wurde, in die Produktionsumgebung zu deployen. Sie muss den Produktions-Image-Build vorbereiten und den Service über manuelles Deployment oder eine automatisierte Pipeline in eine Produktionsumgebung hochladen und testen.

### 8. **Monitoring und Überwachung von Microservices**
**Handlungssituation**: Nach dem Deployment eines neuen Microservices in die Produktion wird festgestellt, dass es zu Leistungseinbrüchen kommt. Lukas richtet Monitoring-Tools wie Prometheus oder Grafana ein, um Metriken wie Antwortzeiten und Fehlerquoten zu überwachen und zu analysieren, um das Problem zu identifizieren und Lösungen vorzuschlagen.

### 9. **Skalierung und Struktur von Microservices verstehen**
**Handlungssituation**: Aufgrund eines Anstiegs von Nutzeranfragen müssen die bestehenden Microservices skaliert werden. Mia entwirft eine skalierbare Struktur für die Anwendung, setzt Load-Balancer ein und konfiguriert Docker-Compose oder Kubernetes, um mehrere Instanzen der Microservices bereitzustellen und die Last dynamisch zu verteilen.

### 10. **Asynchrone Kommunikation und API Gateways einsetzen**
**Handlungssituation**: Ein Microservice, der synchron mit einer externen API kommuniziert, führt zu hohen Latenzzeiten. Leon implementiert eine asynchrone Kommunikation zwischen den Microservices mittels Message-Broker (z. B. RabbitMQ) und nutzt ein API Gateway, um eine effiziente und reaktionsfähige Interaktion zwischen den Diensten zu ermöglichen.