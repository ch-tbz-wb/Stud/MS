![TBZ Logo](./x_gitressourcen/tbz_logo.png) 

# MSVC - Microservices mit Python

## Kurzbeschreibung des Moduls 

Dieses Modul bietet eine umfassende Einführung in die Erstellung und das Management von Microservices unter Verwendung der Programmiersprache Python. Erlernt werden grundlegende Prinzipien, Designansätze und Best Practices für Microservices. Zudem werden fortgeschrittene Themen wie Containerisierung, Orchestrierung, CI/CD-Prozesse und Monitoring behandelt. 
Das Modul richtet sich an Personen, die sich mit den aktuellen Technologien in der Softwarearchitektur vertraut machen möchten. Es bietet praktische Kenntnisse für den effizienten Einsatz von Microservices in realen Projekten und bietet so einen Einstieg in Cloud-native Technologien. 

## Angaben zum Transfer der erworbenen Kompetenzen 

Aufbau einer eigenen Microservice-Komposition aus dem eigenen Berufsumfeld.

## Abhängigkeiten und Abgrenzungen

### Vorangehende Module

* ITCNE - Modul FAAS
* ITCNE - Modul IaC
* ITCNE - Modul AWS

### Nachfolgende Module

* Kubernetes


## Unterlagen zum Modul

### Organisatorisches

[Organisatorisches ](0_Organisatorisches)zur Autorenschaft dieser Dokumente zum Modul

## Handlungskompetenzen

[Handlungskompetenzen](1_Handlungskompetenzen) zu den Handlungszielen 

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen

### Umsetzung

[Umsetzung](3_Umsetzung) 

### Fragekatalog

[Fragekatalog ](4_Fragekatalog) Allgemein

### Handlungssituationen

[Handlungssituationen ](5_Handlungssituationen)mit möglichen Praxissituationen zu den einzelnen Handlungszielen

### Zertifizierungen 

Mögliche [Zertifizierungen](8_Zertifizierungen) für dieses Modul 

- - - 

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

- - - 
